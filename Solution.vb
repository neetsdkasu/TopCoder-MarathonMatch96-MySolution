Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Pos = System.Tuple(Of Integer, Integer)
Imports Tp = System.Tuple(Of Integer, Integer, Integer, Integer)
Imports XYZ = System.Tuple(Of Integer, Integer, Integer)
Imports Bounds = System.Tuple(Of Integer, Integer, Integer, Integer)
Imports Result = System.Tuple(Of Integer(,), Integer())
Imports Tp6 = System.Tuple(Of Integer, Integer, Integer, Integer, Integer, Integer)

Public Class GarlandOfLights
	Dim rand As New Random(19831983)
    
	Const DEBUG_MODE As Boolean = False
    
    Private Function MakeDummy(N As Integer) As Integer()
        Dim dummy(N - 1) As Integer
        For i As Integer = 0 To UBound(dummy)
            dummy(i) = i
        Next i
        MakeDummy = dummy
    End Function
    
	Public Function create(H As Integer, W As Integer, lights() As String) As Integer()
        Dim startTime As Integer = Environment.TickCount
		Dim time0 As Integer = startTime + 9000
		Dim time9 As Integer = time0 + 700
		
		Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))
		
        If H < 10 OrElse W < 10 Then
            create = MakeDummy(H * W)
            Exit Function
        End If
        
        Dim tiles(50) As Stack(Of Integer)
        For j As Integer = 0 To 5
            For i As Integer = 1 To 4
                tiles(i Or (j << 3)) = New Stack(Of Integer)()
            Next i
        Next j
        
        For i As Integer = 0 To UBound(lights)
            Dim v As String = lights(i)
            Dim p As Integer = Asc(v(0)) - Asc("0")
            Dim c As Integer = Asc(v(1)) - Asc("a") + 1
            tiles(c Or (p << 3)).Push(i)
        Next i
        
        Dim colors As Integer = 0
        Dim counts(50) As Integer
        Dim countsA(50) As Integer
        Dim countsT(50) As Integer
        Dim countsH(50) As Integer
        Dim countsI4(50) As Integer
        Dim countsX(50) As Integer
        Dim countsY(50) As Integer
        Dim countsZ(50) As Integer
        Dim countsS(50) As Integer
        Dim countsR(50) As Integer
        Dim wires(50) As Integer
        Dim wiresA(50) As Integer
        Dim wiresT(50) As Integer
        Dim wiresH(50) As Integer
        Dim wiresX(50) As Integer
        Dim wiresY(50) As Integer
        Dim wiresZ(50) As Integer
        Dim wiresS(50) As Integer
        Dim wiresR(50) As Integer
        Dim gap As Integer = 0
        Dim gaps(5) As Integer
        Dim mins(5) As Integer
        For j As Integer = 0 To 5
            Dim tc As Integer = 0
            Dim mx As Integer = 0
            Dim mn As Integer = H * W
            For i As Integer = 1 To 4
                Dim t As Integer = i Or (j << 3)
                counts(t) = tiles(t).Count
                countsA(t) = tiles(t).Count
                countsT(t) = tiles(t).Count
                countsH(t) = tiles(t).Count
                countsI4(t) = tiles(t).Count
                countsX(t) = tiles(t).Count
                countsY(t) = tiles(t).Count
                countsZ(t) = tiles(t).Count
                countsS(t) = tiles(t).Count
                countsR(t) = tiles(t).Count
                wires(j) += tiles(t).Count
                wiresA(j) += tiles(t).Count
                wiresT(j) += tiles(t).Count
                wiresH(j) += tiles(t).Count
                wiresX(j) += tiles(t).Count
                wiresY(j) += tiles(t).Count
                wiresZ(j) += tiles(t).Count
                wiresS(j) += tiles(t).Count
                wiresR(j) += tiles(t).Count
                If counts(t) > 0 Then
                    tc += 1
                    mx = Math.Max(mx, counts(t))
                    mn = Math.Min(mn, counts(t))
                End If
            Next i
            colors = Math.Max(colors, tc)
            gap = Math.Max(gap, mx - mn)
            gaps(j) = mx - mn
            mins(j) = mn
            Console.Error.WriteLine("[{0}]: gap:{1}, min:{2}, max:{3}, sum: {4}", _
                    j, gaps(j), mins(j), mx, wires(j))
        Next j
        
        Console.Error.WriteLine("H: {2}, W: {3}, colors: {0}, gap: {1}, ", _
                colors, gap, H, W)
        
        Dim tmp(H - 1, W - 1) As Integer
        Dim tmpA(H - 1, W - 1) As Integer
        Dim tmpT(H - 1, W - 1) As Integer
        Dim tmpI4(H - 1, W - 1) As Integer
        Dim tmpX(H - 1, W - 1) As Integer
        Dim tmpY(H - 1, W - 1) As Integer
        Dim tmpZ(H - 1, W - 1) As Integer
        Dim tmpS(H - 1, W - 1) As Integer
        Dim tmpR(H - 1, W - 1) As Integer
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                tmp(y, x) = -1
                tmpA(y, x) = -1
                tmpT(y, x) = -1
                tmpI4(y, x) = -1
                tmpX(y, x) = -1
                tmpY(y, x) = -1
                tmpZ(y, x) = -1
                tmpS(y, x) = -1
                tmpR(y, x) = -1
            Next x
        Next y
        
        Dim apc As Integer = 0
        Dim apTimes(9) As Integer
        Dim apScores(9) As Double
        Dim found As Boolean = False
        Dim score As Integer = 0
        Dim selAp As Integer = 0
        
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        Try
            Console.Error.WriteLine("approach {0}", apc)
            Console.Error.WriteLine("try Approach")
            If Approach(H, W, colors, tmpA, countsA, wiresA, mins) Then
                CheckCorrectLoop(H, W, tmpA)
                If Not DEBUG_MODE Then
                    idea4_partial(H, W, colors, tmpA, countsA, Math.Min(time9, Environment.TickCount + 450))
                End If
                score = CalcScore(countsA)
                tmp = tmpA
                counts = countsA
                found = True
                apScores(apc) = CDbl(H * W - score) / CDbl(H * W)
                selAp = apc
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        Try
            Console.Error.WriteLine("approach {0}", apc)
            Console.Error.WriteLine("try ApproachT")
            If ApproachT(H, W, colors, tmpT, countsT, wiresT, mins) Then
                CheckCorrectLoop(H, W, tmpT)
                If Not DEBUG_MODE Then
                    idea4_partial(H, W, colors, tmpT, countsT, Math.Min(time9, Environment.TickCount + 450))
                End If
                Dim tmp_score As Integer = CalcScore(countsT)
                apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                If Not found OrElse tmp_score < score Then
                    tmp = tmpT
                    counts = countsT
                    score = tmp_score
                    selAp = apc
                    found = True
                End If
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        Try
            Console.Error.WriteLine("approach {0}", apc)
            Console.Error.WriteLine("try ApproachX")
            If ApproachX(H, W, colors, tmpX, countsX, wiresX, mins) Then
                CheckCorrectLoop(H, W, tmpX)
                If Not DEBUG_MODE Then
                    idea4_partial(H, W, colors, tmpX, countsX, Math.Min(time9, Environment.TickCount + 300))
                End If
                Dim tmp_score As Integer = CalcScore(countsX)
                apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                If Not found OrElse tmp_score < score Then
                    tmp = tmpX
                    counts = countsX
                    score = tmp_score
                    selAp = apc
                    found = True
                End If
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        Try
            Console.Error.WriteLine("approach {0}", apc)
            Console.Error.WriteLine("try ApproachR")
            If ApproachR(H, W, colors, tmpR, countsR, wiresR, mins) Then
                CheckCorrectLoop(H, W, tmpR)
                If Not DEBUG_MODE Then
                    idea4_partial(H, W, colors, tmpR, countsR, Math.Min(time9, Environment.TickCount + 300))
                End If
                Dim tmp_score As Integer = CalcScore(countsR)
                apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                If Not found OrElse tmp_score < score Then
                    tmp = tmpR
                    counts = countsR
                    score = tmp_score
                    selAp = apc
                    found = True
                End If
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1

        Try
            If apTimes(1) - apTimes(0) < 500 AndAlso apTimes(apc - 1) < startTime + 2000 Then
                Console.Error.WriteLine("approach {0}", apc)
                Console.Error.WriteLine("try ApproachY")
                If ApproachY(H, W, colors, tmpY, countsY, wiresY, mins) Then
                    CheckCorrectLoop(H, W, tmpY)
                    If Not DEBUG_MODE Then
                        idea4_partial(H, W, colors, tmpY, countsY, Math.Min(time9, Environment.TickCount + 300))
                    End If
                    Dim tmp_score As Integer = CalcScore(countsY)
                    apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                    If Not found OrElse tmp_score < score Then
                        tmp = tmpY
                        counts = countsY
                        score = tmp_score
                        selAp = apc
                        found = True
                    End If
                End If
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1

        Try
            If apTimes(1) - apTimes(0) < 500 AndAlso apTimes(apc - 1) < startTime + 3000 Then
                Console.Error.WriteLine("approach {0}", apc)
                Console.Error.WriteLine("try ApproachZ")
                If ApproachZ(H, W, colors, tmpZ, countsZ, wiresZ, mins) Then
                    CheckCorrectLoop(H, W, tmpZ)
                    If Not DEBUG_MODE Then
                        idea4_partial(H, W, colors, tmpZ, countsZ, Math.Min(time9, Environment.TickCount + 300))
                    End If
                    Dim tmp_score As Integer = CalcScore(countsZ)
                    apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                    If Not found OrElse tmp_score < score OrElse DEBUG_MODE Then
                        tmp = tmpZ
                        counts = countsZ
                        score = tmp_score
                        selAp = apc
                        found = True
                    End If
                End If
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        Try
            If apTimes(2) - apTimes(1) < 500 AndAlso apTimes(apc - 1) < startTime + 4000 Then
                Console.Error.WriteLine("approach {0}", apc)
                Console.Error.WriteLine("try ApproachS")
                If ApproachS(H, W, colors, tmpS, countsS, wiresS, mins) Then
                    CheckCorrectLoop(H, W, tmpS)
                    If Not DEBUG_MODE Then
                        idea4_partial(H, W, colors, tmpS, countsS, Math.Min(time9, Environment.TickCount + 300))
                    End If
                    Dim tmp_score As Integer = CalcScore(countsS)
                    apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                    If Not found OrElse tmp_score < score OrElse DEBUG_MODE Then
                        tmp = tmpS
                        counts = countsS
                        score = tmp_score
                        selAp = apc
                        found = True
                    End If
                End If
            End If
        Catch ex As Exception
            Console.Error.WriteLine("ERROR in approach {0}", apc)
            Console.Error.WriteLine(ex)
        End Try
        apTimes(apc) = Environment.TickCount
        apc += 1

        If Not DEBUG_MODE Then
            Try
                Console.Error.WriteLine("approach {0}", apc)
                Console.Error.WriteLine("try Hilbert...")
                Dim rslt As Result = TryHilbert(H, W, colors, tiles, wiresH, countsH, gap, mins, time0, time9)
                If rslt Is Nothing Then
                    Console.Error.WriteLine("failed Hilbert...")
                Else
                    CheckCorrectLoop(H, W, rslt.Item1)
                    Dim tmp_score As Integer = CalcScore(rslt.Item2)
                    apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                    If Not found OrElse tmp_score < score Then
                        tmp = rslt.Item1
                        counts = rslt.Item2
                        score = tmp_score
                        selAp = apc
                        found = True
                    End If
                End If
            Catch ex As Exception
                Console.Error.WriteLine("ERROR in approach {0}", apc)
                Console.Error.WriteLine(ex)
            End Try
        End If
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        If Not DEBUG_MODE Then
            Try
                Console.Error.WriteLine("approach {0}", apc)
                Console.Error.WriteLine("use idea4...")
                idea4(H, W, tmpI4, countsI4, startTime + 9700)
                CheckCorrectLoop(H, W, tmpI4)
                Dim tmp_score As Integer = CalcScore(countsI4)
                apScores(apc) = CDbl(H * W - tmp_score) / CDbl(H * W)
                If Not found OrElse tmp_score < score Then
                    tmp = tmpI4
                    counts = countsI4
                    score = tmp_score
                    selAp = apc
                    found = True
                End If
            Catch ex As Exception
                Console.Error.WriteLine("ERROR in approach {0}", apc)
                Console.Error.WriteLine(ex)
            End Try
        End If
        apTimes(apc) = Environment.TickCount
        apc += 1
        
        For i As Integer = 1 To Math.Min(UBound(apScores), UBound(apTimes))
            Console.Error.WriteLine("Approach {0}'s Score is {1:F15}  time: {2,4}ms.", _
                i, apScores(i), apTimes(i) - apTimes(i - 1))
        Next i
        Console.Error.WriteLine("Select Approach number is {0} !", selAp)
        Console.Error.WriteLine("total time: {0,4}ms ({1,4}ms)", _
            apTimes(UBound(apTimes)) - startTime, _
            apTimes(UBound(apTimes)) - apTimes(0))
        
        Dim ret(H * W - 1) As Integer
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                ret(y * W + x) = tmp(y, x)
            Next x
        Next y
        
        Dim idx As Integer = 0
        For j As Integer = 0 To 5
            For i As Integer = 1 To 4
                Dim t As Integer = i Or (j << 3)
                For k As Integer = 1 To counts(t)
                    Do While ret(idx) >= 0
                        idx += 1
                    Loop
                    ret(idx) = t
                Next k
            Next i
        Next j
        
        For i As Integer = 0 To UBound(ret)
            ret(i) = tiles(ret(i)).Pop()
        Next i
        
		create = ret
		
	End Function
    
    Private Function CheckCorrectLoop(H As Integer, W As Integer, tmp(,) As Integer) As Boolean
        CheckCorrectLoop = False
        Dim sx As Integer = 0, sy As Integer = 0, cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) < 0 Then Continue For
                sx = x: sy = y
                cnt += 1
            Next x
        Next y
        If cnt = 0 Then Exit Function
        Dim face As Integer
        ' 0-N(^) 1-E(>) 2-S(v) 3-W(<) (goto at before place)
        Select Case tmp(sy, sx) >> 3
        Case 0: face = 0 ' from south to north (^)
        Case 1: face = 0
        Case 2: face = 2
        Case 3: face = 2
        Case 4: face = 1 ' from west to east (>)
        Case 5: face = 2
        End Select
        Dim cx As Integer = sx, cy As Integer = sy
        Dim i As Integer = 1
        For i = 1 To cnt
            Select Case tmp(cy, cx) >> 3
            Case 0
                If cx + 1 >= W OrElse cy + 1 >= H Then Exit For
                Select Case face
                Case 0
                    Select Case tmp(cy, cx + 1) >> 3
                    Case 1, 2, 4
                        If (tmp(cy, cx) And 7) = (tmp(cy, cx + 1) And 7) Then Exit For
                        cx += 1: face = 1
                    Case Else: Exit For
                    End Select
                Case 3
                    Select Case tmp(cy + 1, cx) >> 3
                    Case 2, 3, 5
                        If (tmp(cy, cx) And 7) = (tmp(cy + 1, cx) And 7) Then Exit For
                        cy += 1: face = 2
                    Case Else: Exit For
                    End Select
                Case Else: Exit For
                End Select
            Case 1
                If cx - 1 < 0 OrElse cy + 1 >= H Then Exit For
                Select Case face
                Case 0
                    Select Case tmp(cy, cx - 1) >> 3
                    Case 0, 3, 4
                        If (tmp(cy, cx) And 7) = (tmp(cy, cx - 1) And 7) Then Exit For
                        cx -= 1: face = 3
                    Case Else: Exit For
                    End Select
                Case 1
                    Select Case tmp(cy + 1, cx) >> 3
                    Case 2, 3, 5
                        If (tmp(cy, cx) And 7) = (tmp(cy + 1, cx) And 7) Then Exit For
                        cy += 1: face = 2
                    Case Else: Exit For
                    End Select
                Case Else: Exit For
                End Select
            Case 2
                If cx - 1 < 0 OrElse cy - 1 < 0 Then Exit For
                Select Case face
                Case 2
                    Select Case tmp(cy, cx - 1) >> 3
                    Case 0, 3, 4
                        If (tmp(cy, cx) And 7) = (tmp(cy, cx - 1) And 7) Then Exit For
                        cx -= 1: face = 3
                    Case Else: Exit For
                    End Select
                Case 1
                    Select Case tmp(cy - 1, cx) >> 3
                    Case 0, 1, 5
                        If (tmp(cy, cx) And 7) = (tmp(cy - 1, cx) And 7) Then Exit For
                        cy -= 1: face = 0
                    Case Else: Exit For
                    End Select
                Case Else: Exit For
                End Select
            Case 3
                If cx + 1 >= W OrElse cy - 1 < 0 Then Exit For
                Select Case face
                Case 2
                    Select Case tmp(cy, cx + 1) >> 3
                    Case 1, 2, 4
                        If (tmp(cy, cx) And 7) = (tmp(cy, cx + 1) And 7) Then Exit For
                        cx += 1: face = 1
                    Case Else: Exit For
                    End Select
                Case 3
                    Select Case tmp(cy - 1, cx) >> 3
                    Case 0, 1, 5
                        If (tmp(cy, cx) And 7) = (tmp(cy - 1, cx) And 7) Then Exit For
                        cy -= 1: face = 0
                    Case Else: Exit For
                    End Select
                Case Else: Exit For
                End Select
            Case 4
                If cx + 1 >= W OrElse cx - 1 < 0 Then Exit For
                Select Case face
                Case 1
                    Select Case tmp(cy, cx + 1) >> 3
                    Case 1, 2, 4
                        If (tmp(cy, cx) And 7) = (tmp(cy, cx + 1) And 7) Then Exit For
                        cx += 1: face = 1
                    Case Else: Exit For
                    End Select
                Case 3
                    Select Case tmp(cy, cx - 1) >> 3
                    Case 0, 3, 4
                        If (tmp(cy, cx) And 7) = (tmp(cy, cx - 1) And 7) Then Exit For
                        cx -= 1: face = 3
                    Case Else: Exit For
                    End Select
                Case Else: Exit For
                End Select
            Case 5
                If cy + 1 >= H OrElse cy - 1 < 0 Then Exit For
                Select Case face
                Case 0
                    Select Case tmp(cy - 1, cx) >> 3
                    Case 0, 1, 5
                        If (tmp(cy, cx) And 7) = (tmp(cy - 1, cx) And 7) Then Exit For
                        cy -= 1: face = 0
                    Case Else: Exit For
                    End Select
                Case 2
                    Select Case tmp(cy + 1, cx) >> 3
                    Case 2, 3, 5
                        If (tmp(cy, cx) And 7) = (tmp(cy + 1, cx) And 7) Then Exit For
                        cy += 1: face = 2
                    Case Else: Exit For
                    End Select
                Case Else: Exit For
                End Select
            End Select
        
        Next i
        
        If i > cnt AndAlso (cx = sx) And (cy = sy) Then
            Console.Error.WriteLine("correct loop")
            CheckCorrectLoop = True
        Else
            Console.Error.WriteLine( _
                "incorrect loop: i:{0}, cx:{1}, cy{2}, sx{3}, sy{4}, face:{5}, tmp:{6}", _
                    i, cx, cy, sx, sy, face, _
                    if(cx >= 0 And cx < W And cy >= 0 And cy < H, CStr(tmp(cy, cx)), "NaN"))
            Throw New Exception("No Loop!!")
        End If

    End Function
    
    Private Function Approach(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        Dim sx As Integer = 0, sy As Integer = H - 1, sf As Integer = 1
        Dim gx As Integer = W - 1, gy As Integer = 0
        If wires(4) \ (W - 2) > 2 Then
            Dim hr As Integer = (wires(4) \ (W - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim y1 As Integer = i + i + 1
                Dim y2 As Integer = y1 + 1
                tmp(y1, 1) = 0
                tmp(y2, 1) = 3
                tmp(y1, W - 1) = 2
                tmp(y2, W - 1) = 1
                For x As Integer = 2 To W - 2
                    tmp(y1, x) = 4
                    tmp(y2, x) = 4
                Next x
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(4) -= hr * 2 * (W - 3)
            gy = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (W - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W Then Exit Do
                For x As Integer = 2 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 1) = 0: tmp(gy + 2, 1) = 5
                tmp(gy + 3, 1) = 5: tmp(gy + 4, 1) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W - 2: wires(1) -= W - 2
                wires(2) -= W - 2: wires(3) -= W - 2
                wires(5) -= 4
                gy += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W + 2 Then Exit Do
                For x As Integer = 3 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 0) = 3: tmp(gy + 1, 1) = 1
                tmp(gy + 2, 0) = 0: tmp(gy + 2, 1) = 2
                tmp(gy + 3, 0) = 3: tmp(gy + 3, 1) = 1
                tmp(gy + 4, 0) = 0: tmp(gy + 4, 1) = 2
                tmp(gy + 1, 2) = 0: tmp(gy + 2, 2) = 5
                tmp(gy + 3, 2) = 5: tmp(gy + 4, 2) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W: wires(1) -= W
                wires(2) -= W: wires(3) -= W
                gy += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gy < H - 4 AndAlso wires(5) \ (H - (gy + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(5) \ (H - (gy + 1) - 1) - 1) \ 2
            Dim bt As Integer = H - 4
            If ((W - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 1
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2: wires(5) -= vr * 2 * (bt - gy + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 2
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2 + 1: wires(5) -= vr * 2 * (bt - gy + 1)
            End If
        End If
        
        Dim wr As Integer
        If colors = 2 Then
            wr = Math.Min(wires(0) \ 2, Math.Min(wires(1) \ 2, Math.Min(wires(2) \ 2 - 1, wires(3) \ 2)))
        Else
            wr = Math.Min(wires(0), Math.Min(wires(1), Math.Min(wires(2) - 1, wires(3))))
        End If
        Do While wr > 0
            Dim f As Boolean = True
            If H - (gy + 1) > 1 AndAlso W - (sx + 1) > 5 AndAlso wires(4) > 2 Then
                tmp(H - 2, sx + 1) = 0
                tmp(H - 1, sx + 1) = 2
                tmp(H - 2, sx + 2) = 4
                tmp(H - 2, sx + 3) = 1
                tmp(H - 1, sx + 3) = 3
                tmp(H - 1, sx + 4) = 4
                sx += 4
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1: wires(4) -= 2
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If W - (sx + 1) > 1 AndAlso H - (gy + 1) > 5 AndAlso wires(5) > 2 Then
                tmp(gy + 1, W - 1) = 2
                tmp(gy + 1, W - 2) = 0
                tmp(gy + 2, W - 2) = 5
                tmp(gy + 3, W - 2) = 3
                tmp(gy + 3, W - 1) = 1
                tmp(gy + 4, W - 1) = 5
                gy += 4
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1: wires(5) -= 2
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop

        Do While wr > 0
            Dim f As Boolean = True
            If H - (gy + 1) > 1 AndAlso W - (sx + 1) > 3 Then
                tmp(H - 2, sx + 1) = 0
                tmp(H - 1, sx + 1) = 2
                tmp(H - 2, sx + 2) = 1
                tmp(H - 1, sx + 2) = 3
                sx += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If W - (sx + 1) > 1 AndAlso H - (gy + 1) > 3 Then
                tmp(gy + 1, W - 1) = 2
                tmp(gy + 1, W - 2) = 0
                tmp(gy + 2, W - 2) = 3
                tmp(gy + 2, W - 1) = 1
                gy += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop
        
        For x As Integer = sx + 1 To W - 2
            tmp(H - 1, x) = 4
        Next x
        For y As Integer = gy + 1 To H - 2
            tmp(y, W - 1) = 5
        Next y
        tmp(H - 1, W - 1) = 2
        sx = W - 2: gy = H - 1
        
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            Console.Error.WriteLine("found it!")
            Approach = True
            Exit Function
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            Approach = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: Approach = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        For i As Integer = 1 To 10
            InitDfs(200)
            If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
                Console.Error.WriteLine("dfs ... {0}", i)
                Approach = True: Exit Function
            End If
            
            If i > 1 AndAlso sx - gx >= W \ 4 AndAlso sx > 5 Then
                For j As Integer = 1 To 4
                    Dim tt As Integer = tmp(sy, sx)
                    If tt < 0 Then
                        Approach = False: Exit Function
                    End If
                    tmp(sy, sx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx += 1
                    Case 1: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx -= 1
                    Case 2: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx -= 1
                    Case 3: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx += 1
                    Case 4: If tmp(sy, sx + 1) >= 0 Then sx += 1 Else sx -= 1
                    Case 5: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sy -= 1
                    Case Else: Approach = False: Exit Function
                    End Select
                Next j
                Select Case tmp(sy, sx) >> 3
                Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
                Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
                Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
                Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
                Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
                Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
                Case Else: Approach = False: Exit Function
                End Select
            Else
                For j As Integer = 1 To 3
                    Dim tt As Integer = tmp(gy, gx)
                    If tt < 0 Then
                        Approach = False: Exit Function
                    End If
                    tmp(gy, gx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx += 1
                    Case 1: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx -= 1
                    Case 2: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx -= 1
                    Case 3: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx += 1
                    Case 4: If tmp(gy, gx + 1) >= 0 Then gx += 1 Else gx -= 1
                    Case 5: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gy -= 1
                    Case Else: Approach = False: Exit Function
                    End Select
                Next j
            End If
        Next i
        
        If DEBUG_MODE Then
            Approach = True: Exit Function
        End If
        
        Approach = False
        
    End Function
    
    Private Function Dfs(depth As Integer, H As Integer, W As Integer, _
            tmp(,) As Integer, counts() As Integer, _
            fc As Integer, curX as Integer, curY As Integer, goal As Pos, wrong As Integer) As Boolean
        Dfs = False
        If depth > H + W + 20 Then
            'Dfs = True
            Exit Function
        End If
        If dfsOver Then Exit Function
        dfsCount += 1
        If dfsCount > 10000 Then
            Dim now_time As Integer = Environment.TickCount
            If now_time > dfsLimit Then
                Console.Error.WriteLine("time out Dfs")
                dfsOver = True
                ' If DEBUG_MODE Then Dfs = True ' DEBUG
                Exit Function
            End If
        End If
        Dim t As Integer = tmp(curY, curX) >> 3
        Dim c As Integer = tmp(curY, curX) And 7
        Dim toX As Integer = curX, toY As Integer = curY
        Dim es() As Pos
        Select Case fc
        Case 0: toY -= 1: es = north
        case 1: toX += 1: es = east
        case 2: toY += 1: es = south
        case 3: toX -= 1: es = west
        Case Else: Exit Function
        End Select
        If toX < 0 OrElse toY < 0 OrElse toX >= W OrElse toY >= H Then Exit Function
        IF toX = goal.Item1 AndAlso toY = goal.Item2 Then
            Dim g As Integer = tmp(goal.Item2, goal.Item1)
            Select Case fc
            Case 0
                Select Case g >> 3
                Case 0, 1, 5: Dfs = (g And 7) <> c
                End Select
            Case 1
                Select Case g >> 3
                Case 1, 2, 4: Dfs = (g And 7) <> c
                End Select
            Case 2
                Select Case g >> 3
                Case 2, 3, 5: Dfs = (g And 7) <> c
                End Select
            Case 3
                Select Case g >> 3
                Case 0, 3, 4: Dfs = (g And 7) <> c
                End Select
            End Select
            Exit Function
        End If
        If tmp(toY, toX) >= 0 Then Exit Function
        
        Dim aimH As Integer = If(toX < goal.Item1, 1, If(toX > goal.Item1, 3, -1))
        Dim aimV As Integer = If(toY < goal.Item2, 2, If(toY > goal.Item2, 0, -1))
        For i As Integer = 1 To 4
            If i = c Then Continue For
            For Each e As Pos In es
                If e.Item2 <> aimH AndAlso e.Item2 <> aimV Then Continue For
                Dim u As Integer = i Or (e.Item1 << 3)
                If counts(u) = 0 Then Continue For
                counts(u) -= 1
                tmp(toY, toX) = u
                If Dfs(depth + 1, H, W, tmp, counts, e.Item2, toX, toY, goal, wrong) Then
                    Dfs = True: Exit Function
                End If
                counts(u) += 1
            Next e
        Next i
        If wrong < 5 Then
            For i As Integer = 1 To 4
                If i = c Then Continue For
                For Each e As Pos In es
                    If e.Item2 = aimH OrElse e.Item2 = aimV Then Continue For
                    Dim u As Integer = i Or (e.Item1 << 3)
                    If counts(u) = 0 Then Continue For
                    counts(u) -= 1
                    tmp(toY, toX) = u
                    If Dfs(depth + 1, H, W, tmp, counts, e.Item2, toX, toY, goal, wrong + 1) Then
                        Dfs = True: Exit Function
                    End If
                    counts(u) += 1
                Next e
            Next i
        End If
        tmp(toY, toX) = -1
        
    End Function
    
    Private Function TryPlace(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, _
            first As Integer, start As Pos, goal As Pos) As Pos
        ' first: 0-N, 1-E, 2-S, 3-W (come from)
        Dim stock(5, 4) As LinkedList(Of Pos)
        For i As Integer = 0 To 5
            For j As Integer = 1 To 4
                stock(i, j) = New LinkedList(Of Pos)()
            Next j
        Next i
        Dim cols(H - 1, W - 1) As Integer
        Dim cx As Integer = start.Item1, cy As Integer = start.Item2, face As Integer = first
        Dim flag As Integer = 0
        Dim tmp_total As Integer = 0
        Dim total As Integer = 0
        Dim time0 As Integer = Environment.TickCount + 2000
        ' Dim LP As Integer = 0
        Do
            ' LP += 1: Console.Error.Write("[{0},({1},{2}),{3}], ", LP, cx, cy, tmp(cy, cx))
            If Environment.TickCount > time0 Then 
                Console.Error.WriteLine("time over")
                TestFix(H, W, tmp, cols, counts)
                TryPlace = New Pos(cx, cy): Exit Function
            End If
            If cy < 0 OrElse cx < 0 OrElse cy >= H OrElse cx >= W Then
                Console.Error.WriteLine("out of bounds: cx:{0}, cy:{1}, fase:{2}", cx, cy, face)
                cx = Math.Max(0, Math.Min(cx, W - 1))
                cy = Math.Max(0, Math.Min(cy, H - 1))
                TestFix(H, W, tmp, cols, counts)
                TryPlace = New Pos(cx, cy): Exit Function
            End If
            Dim t As Integer = tmp(cy, cx)
            Dim omit1, omit2 As Integer
            Select Case t
            Case 0: omit1 = cols(cy + 1, cx): omit2 = cols(cy, cx + 1): face = If(face = 2, 3, 0)
            Case 1: omit1 = cols(cy + 1, cx): omit2 = cols(cy, cx - 1): face = If(face = 2, 1, 0)
            Case 2: omit1 = cols(cy - 1, cx): omit2 = cols(cy, cx - 1): face = If(face = 0, 1, 2)
            Case 3: omit1 = cols(cy - 1, cx): omit2 = cols(cy, cx + 1): face = If(face = 0, 3, 2)
            Case 4: omit1 = cols(cy, cx + 1): omit2 = cols(cy, cx - 1): ' face = face
            Case 5: omit1 = cols(cy + 1, cx): omit2 = cols(cy - 1, cx): ' face = face
            Case Else
                Console.Error.WriteLine("bad value: x:{0}, y:{1}, t:{2}", cx, cy, t)
                TestFix(H, W, tmp, cols, counts)
                TryPlace = New Pos(cx, cy): Exit Function
            End Select
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = omit1 OrElse i = omit2 Then Continue For
                If counts(i Or (t << 3)) > selc Then
                    selc = counts(i Or (t << 3))
                    seli = i
                    If colors = 2 AndAlso flag < 2 Then
                        Exit For
                    End If
                End If
            Next i
            If seli = 0 Then
                If colors = 2 AndAlso flag = 0 Then
                    Console.Error.WriteLine("hoge")
                    For y As Integer = 0 To H - 1
                        For x As Integer = 0 To W - 1
                            If cols(y, x) = 0 Then Continue For
                            counts(cols(y, x) Or (tmp(y, x) << 3)) += 1
                            cols(y, x) = 0
                        Next x
                    Next y
                    total = tmp_total
                    cx = start.Item1: cy = start.Item2: face = first: tmp_total = 0
                    flag = 2: Continue Do
                ElseIf colors = 2 AndAlso flag = 2 AndAlso tmp_total < total Then
                    Console.Error.WriteLine("hoge2")
                    For y As Integer = 0 To H - 1
                        For x As Integer = 0 To W - 1
                            If cols(y, x) = 0 Then Continue For
                            counts(cols(y, x) Or (tmp(y, x) << 3)) += 1
                            cols(y, x) = 0
                        Next x
                    Next y
                    cx = start.Item1: cy = start.Item2: face = first: tmp_total = 0
                    flag = 1: Continue Do
                ElseIf colors > 2 Then
                    Dim dx1 As Integer = 0, dy1 As Integer = 0
                    Dim dx2 As Integer = 0, dy2 As Integer = 0
                    Select Case t
                    Case 0: dy1 =  1: dx2 =  1
                    Case 1: dy1 =  1: dx2 = -1
                    Case 2: dy1 = -1: dx2 = -1
                    Case 3: dy1 = -1: dx2 =  1
                    Case 4: dx1 = -1: dx2 =  1
                    Case 5: dy1 =  1: dy2 = -1
                    End Select
                    For i As Integer = 1 To 4
                        If i = omit1 OrElse i = omit2 Then Continue For
                        If stock(t, i).Count = 0 Then Continue For
                        Dim node As LinkedListNode(Of Pos) = stock(t, i).First
                        Do While node IsNot Nothing
                            Dim p As Pos = node.Value
                            Dim om3 As Integer = cols(p.Item2 + dy1, p.Item1 + dx1)
                            Dim om4 As Integer = cols(p.Item2 + dy2, p.Item1 + dx2)
                            For j As Integer = 1 To 4
                                If j = i OrElse j = om3 OrElse j = om4 Then Continue For
                                If counts(j Or (t << 3)) = 0 Then Continue For
                                seli = i
                                counts(i Or (t << 3)) += 1
                                node.List.Remove(node)
                                cols(p.Item2, p.Item1) = j
                                counts(j Or (t << 3)) -= 1
                                stock(t, j).AddLast(p)
                                GoTo OuterLoop
                            Next j
                            node = node.Next
                        Loop
                    Next i
OuterLoop:
                    If seli = 0 Then
                        Console.Error.WriteLine("piyo")
                        TestFix(H, W, tmp, cols, counts)
                        TryPlace = New Pos(cx, cy): Exit Function
                    End IF
                Else
                    Console.Error.WriteLine("fuga")
                    TestFix(H, W, tmp, cols, counts)
                    TryPlace = New Pos(cx, cy): Exit Function
                End If
            End If
            tmp_total += 1
            counts(seli Or (t << 3)) -= 1
            cols(cy, cx) = seli
            stock(t, seli).AddLast(New Pos(cx, cy))
            If cx = goal.Item1 And cy = goal.Item2 Then Exit Do
            Select Case face
            Case 0: cy += 1
            Case 1: cx -= 1 
            Case 2: cy -= 1
            Case 3: cx += 1
            End Select
        Loop
        
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) < 0 Then Continue For
                tmp(y, x) = cols(y, x) Or (tmp(y, x) << 3)
            Next x
        Next y
        
        TryPlace = Nothing
    End Function
    
    Private Sub TestFix(H As Integer, W As Integer, tmp(,) As Integer, cols(,) As Integer, counts() As Integer)
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) < 0 Then Continue For
                If cols(y, x) = 0 Then
                    tmp(y, x) = -1
                Else
                    tmp(y, x) = cols(y, x) Or (tmp(y, x) << 3)
                    ' counts(cols(y, x) Or (tmp(y, x) << 3)) += 1
                End If
            Next x
        Next y        
    End Sub
    
    Private Function CheckCounts(counts() As Integer) As Boolean
        CheckCounts = True
        For i As Integer = 0 To 5
            For j As Integer = 1 To 4
                If counts(j Or (i << 3)) < 0 Then
                    Console.Error.WriteLine( _
                        "illigal counts: t:{0}, col:{1}, count:{2} (col OR (t<<3)={3}", _
                        i, j, counts(j Or (i << 3)), j Or (i << 3))
                    CheckCounts = False
                End If
            Next j
        Next i
    End Function
    
    Private Function ExtendToRight(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToRight = False
        If tmp(y, x + 1) >= 0 OrElse tmp(y + 1, x + 1) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y,     x)
        Dim t2 As Integer = tmp(y + 1, x): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 1 AndAlso w1 <> 5 Then Exit Function
        If w2 <> 2 AndAlso w2 <> 5 Then Exit Function
        Dim oa As Integer = If(w1 = 1, tmp(y,     x - 1), tmp(y - 1, x)) And 7
        Dim oc As Integer = If(w2 = 2, tmp(y + 1, x - 1), tmp(y + 2, x)) And 7
        Dim wa As Integer = If(w1 = 1, 4 << 3, 3 << 3)
        Dim wc As Integer = If(w2 = 2, 4 << 3, 0 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp = SearchQuartetR(counts, New Tp(wa, 1 << 3, wc, 2 << 3), New Tp(oa, 0, oc, 0))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceQuartet(tmp, counts, x, y, cols)
        ExtendToRight = True
    End Function

    Private Function ExtendToLeft(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToLeft = False
        If tmp(y, x - 1) >= 0 OrElse tmp(y + 1, x - 1) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y,     x)
        Dim t2 As Integer = tmp(y + 1, x): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 0 AndAlso w1 <> 5 Then Exit Function
        If w2 <> 3 AndAlso w2 <> 5 Then Exit Function
        Dim ob As Integer = If(w1 = 0, tmp(y,     x + 1), tmp(y - 1, x)) And 7
        Dim od As Integer = If(w2 = 3, tmp(y + 1, x + 1), tmp(y + 2, x)) And 7
        Dim wb As Integer = If(w1 = 0, 4 << 3, 2 << 3)
        Dim wd As Integer = If(w2 = 3, 4 << 3, 1 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp = SearchQuartetL(counts, New Tp(0 << 3, wb, 3 << 3, wd), New Tp(0, ob, 0, od))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceQuartet(tmp, counts, x - 1, y, cols)
        ExtendToLeft = True
    End Function

    Private Function ExtendToTop(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToTop = False
        If tmp(y - 1, x) >= 0 OrElse tmp(y - 1, x + 1) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y, x    )
        Dim t2 As Integer = tmp(y, x + 1): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 0 AndAlso w1 <> 4 Then Exit Function
        If w2 <> 1 AndAlso w2 <> 4 Then Exit Function
        Dim oc As Integer = If(w1 = 0, tmp(y + 1, x    ), tmp(y, x - 1)) And 7
        Dim od As Integer = If(w2 = 1, tmp(y + 1, x + 1), tmp(y, x + 2)) And 7
        Dim wc As Integer = If(w1 = 0, 5 << 3, 2 << 3)
        Dim wd As Integer = If(w2 = 1, 5 << 3, 3 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp = SearchQuartetT(counts, New Tp(0 << 3, 1 << 3, wc, wd), New Tp(0, 0, oc, od))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceQuartet(tmp, counts, x, y - 1, cols)
        ExtendToTop = True
    End Function
    
    Private Function ExtendToBottom(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToBottom = False
        If tmp(y + 1, x) >= 0 OrElse tmp(y + 1, x + 1) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y, x    )
        Dim t2 As Integer = tmp(y, x + 1): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 3 AndAlso w1 <> 4 Then Exit Function
        If w2 <> 2 AndAlso w2 <> 4 Then Exit Function
        Dim oa As Integer = If(w1 = 3, tmp(y - 1, x    ), tmp(y, x - 1)) And 7
        Dim ob As Integer = If(w2 = 2, tmp(y - 1, x + 1), tmp(y, x + 2)) And 7
        Dim wa As Integer = If(w1 = 3, 5 << 3, 1 << 3)
        Dim wb As Integer = If(w2 = 2, 5 << 3, 0 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp = SearchQuartetB(counts, New Tp(wa, wb, 3 << 3, 2 << 3), New Tp(oa, ob, 0, 0))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceQuartet(tmp, counts, x, y, cols)
        ExtendToBottom = True
    End Function
        
    Private Function ExtendToRight2(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToRight2 = False
        If tmp(y, x + 1) >= 0 OrElse tmp(y + 1, x + 1) >= 0 Then Exit Function
        If tmp(y, x + 2) >= 0 OrElse tmp(y + 1, x + 2) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y,     x)
        Dim t2 As Integer = tmp(y + 1, x): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 1 AndAlso w1 <> 5 Then Exit Function
        If w2 <> 2 AndAlso w2 <> 5 Then Exit Function
        Dim oa As Integer = If(w1 = 1, tmp(y,     x - 1), tmp(y - 1, x)) And 7
        Dim oc As Integer = If(w2 = 2, tmp(y + 1, x - 1), tmp(y + 2, x)) And 7
        Dim wa As Integer = If(w1 = 1, 4 << 3, 3 << 3)
        Dim wc As Integer = If(w2 = 2, 4 << 3, 0 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp6 = SearchSextetR(counts, New Tp(wa, 1 << 3, wc, 2 << 3), New Tp(oa, 0, oc, 0))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceSextetH(tmp, counts, x, y, cols)
        ExtendToRight2 = True
    End Function
    
    Private Function ExtendToLeft2(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToLeft2 = False
        If tmp(y, x - 1) >= 0 OrElse tmp(y + 1, x - 1) >= 0 Then Exit Function
        If tmp(y, x - 2) >= 0 OrElse tmp(y + 1, x - 2) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y,     x)
        Dim t2 As Integer = tmp(y + 1, x): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 0 AndAlso w1 <> 5 Then Exit Function
        If w2 <> 3 AndAlso w2 <> 5 Then Exit Function
        Dim ob As Integer = If(w1 = 0, tmp(y,     x + 1), tmp(y - 1, x)) And 7
        Dim od As Integer = If(w2 = 3, tmp(y + 1, x + 1), tmp(y + 2, x)) And 7
        Dim wb As Integer = If(w1 = 0, 4 << 3, 2 << 3)
        Dim wd As Integer = If(w2 = 3, 4 << 3, 1 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp6 = SearchSextetL(counts, New Tp(0 << 3, wb, 3 << 3, wd), New Tp(0, ob, 0, od))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceSextetH(tmp, counts, x - 2, y, cols)
        ExtendToLeft2 = True
        ' Console.Error.Write("L!")
    End Function

    Private Function ExtendToTop2(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToTop2 = False
        If tmp(y - 1, x) >= 0 OrElse tmp(y - 1, x + 1) >= 0 Then Exit Function
        If tmp(y - 2, x) >= 0 OrElse tmp(y - 2, x + 1) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y, x    )
        Dim t2 As Integer = tmp(y, x + 1): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 0 AndAlso w1 <> 4 Then Exit Function
        If w2 <> 1 AndAlso w2 <> 4 Then Exit Function
        Dim oc As Integer = If(w1 = 0, tmp(y + 1, x    ), tmp(y, x - 1)) And 7
        Dim od As Integer = If(w2 = 1, tmp(y + 1, x + 1), tmp(y, x + 2)) And 7
        Dim wc As Integer = If(w1 = 0, 5 << 3, 2 << 3)
        Dim wd As Integer = If(w2 = 1, 5 << 3, 3 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp6 = SearchSextetT(counts, New Tp(0 << 3, 1 << 3, wc, wd), New Tp(0, 0, oc, od))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceSextetV(tmp, counts, x, y - 2, cols)
        ExtendToTop2 = True
        ' Console.Error.Write("T!")
    End Function
    
    Private Function ExtendToBottom2(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExtendToBottom2 = False
        If tmp(y + 1, x) >= 0 OrElse tmp(y + 1, x + 1) >= 0 Then Exit Function
        If tmp(y + 2, x) >= 0 OrElse tmp(y + 2, x + 1) >= 0 Then Exit Function
        Dim t1 As Integer = tmp(y, x    )
        Dim t2 As Integer = tmp(y, x + 1): If t2 < 0 Then Exit Function
        Dim w1 As Integer = t1 >> 3
        Dim w2 As Integer = t2 >> 3
        If w1 <> 3 AndAlso w1 <> 4 Then Exit Function
        If w2 <> 2 AndAlso w2 <> 4 Then Exit Function
        Dim oa As Integer = If(w1 = 3, tmp(y - 1, x    ), tmp(y, x - 1)) And 7
        Dim ob As Integer = If(w2 = 2, tmp(y - 1, x + 1), tmp(y, x + 2)) And 7
        Dim wa As Integer = If(w1 = 3, 5 << 3, 1 << 3)
        Dim wb As Integer = If(w2 = 2, 5 << 3, 0 << 3)
        counts(t1) += 1: counts(t2) += 1
        Dim cols As Tp6 = SearchSextetB(counts, New Tp(wa, wb, 3 << 3, 2 << 3), New Tp(oa, ob, 0, 0))
        If cols Is Nothing Then
            counts(t1) -= 1: counts(t2) -= 1
            Exit Function
        End If
        PlaceSextetV(tmp, counts, x, y, cols)
        ExtendToBottom2 = True
        ' Console.Error.WRite("B!")
    End Function
    
    Private Sub PlaceQuartet(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer, cols As Tp)
        tmp(y,     x    ) = cols.Item1
        tmp(y,     x + 1) = cols.Item2
        tmp(y + 1, x    ) = cols.Item3
        tmp(y + 1, x + 1) = cols.Item4
        counts(cols.Item1) -= 1
        counts(cols.Item2) -= 1
        counts(cols.Item3) -= 1
        counts(cols.Item4) -= 1
    End Sub

    Private Sub PlaceSextetH(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer, cols As Tp6)
        tmp(y,     x    ) = cols.Item1
        tmp(y,     x + 2) = cols.Item2
        tmp(y + 1, x    ) = cols.Item3
        tmp(y + 1, x + 2) = cols.Item4
        tmp(y    , x + 1) = cols.Item5
        tmp(y + 1, x + 1) = cols.Item6
        counts(cols.Item1) -= 1
        counts(cols.Item2) -= 1
        counts(cols.Item3) -= 1
        counts(cols.Item4) -= 1
        counts(cols.Item5) -= 1
        counts(cols.Item6) -= 1
    End Sub
    
    Private Sub PlaceSextetV(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer, cols As Tp6)
        tmp(y,     x    ) = cols.Item1
        tmp(y,     x + 1) = cols.Item2
        tmp(y + 2, x    ) = cols.Item3
        tmp(y + 2, x + 1) = cols.Item4
        tmp(y + 1, x    ) = cols.Item5
        tmp(y + 1, x + 1) = cols.Item6
        counts(cols.Item1) -= 1
        counts(cols.Item2) -= 1
        counts(cols.Item3) -= 1
        counts(cols.Item4) -= 1
        counts(cols.Item5) -= 1
        counts(cols.Item6) -= 1
    End Sub

    Private Function SearchQuartet(counts() As Integer, ws As Tp, os As Tp) As Tp
        ' a - b
        ' |   |
        ' c - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For b As Integer = 1 To 4
                If b = a OrElse b = os.Item2 Then Continue For
                If counts(b Or ws.Item2) = 0 Then Continue For
                For c As Integer = 1 To 4
                    If c = a OrElse c = os.Item3 Then Continue For
                    If counts(c Or ws.Item3) = 0 Then Continue For
                    For d As Integer = 1 To 4
                        If d = b OrElse d = c OrElse d = os.item4 Then Continue For
                        If counts(d Or ws.Item4) = 0 Then Continue For
                        SearchQuartet = New Tp(a Or ws.Item1, b Or ws.Item2, c Or ws.Item3, d Or ws.Item4)
                        Exit Function
                    Next d
                Next c
            Next b
        Next a
        SearchQuartet = Nothing
    End Function

    Private Function SearchQuartetT(counts() As Integer, ws As Tp, os As Tp) As Tp
        ' a - b
        ' |   |
        ' c   d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For b As Integer = 1 To 4
                If b = a OrElse b = os.Item2 Then Continue For
                If counts(b Or ws.Item2) = 0 Then Continue For
                For c As Integer = 1 To 4
                    If c = a OrElse c = os.Item3 Then Continue For
                    If counts(c Or ws.Item3) = 0 Then Continue For
                    For d As Integer = 1 To 4
                        If d = b OrElse d = os.item4 Then Continue For
                        If counts(d Or ws.Item4) = 0 Then Continue For
                        If c = d AndAlso ws.Item3 = ws.Item4 AndAlso counts(d Or ws.Item4) < 2 Then Continue For
                        SearchQuartetT = New Tp(a Or ws.Item1, b Or ws.Item2, c Or ws.Item3, d Or ws.Item4)
                        Exit Function
                    Next d
                Next c
            Next b
        Next a
        SearchQuartetT = Nothing
    End Function

    Private Function SearchQuartetB(counts() As Integer, ws As Tp, os As Tp) As Tp
        ' a   b
        ' |   |
        ' c - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For b As Integer = 1 To 4
                If b = os.Item2 Then Continue For
                If counts(b Or ws.Item2) = 0 Then Continue For
                If a = b AndAlso ws.Item1 = ws.Item2 AndAlso counts(b Or ws.Item2) < 2 Then Continue For
                For c As Integer = 1 To 4
                    If c = a OrElse c = os.Item3 Then Continue For
                    If counts(c Or ws.Item3) = 0 Then Continue For
                    For d As Integer = 1 To 4
                        If d = b OrElse d = c OrElse d = os.item4 Then Continue For
                        If counts(d Or ws.Item4) = 0 Then Continue For
                        SearchQuartetB = New Tp(a Or ws.Item1, b Or ws.Item2, c Or ws.Item3, d Or ws.Item4)
                        Exit Function
                    Next d
                Next c
            Next b
        Next a
        SearchQuartetB = Nothing
    End Function
    
    Private Function SearchQuartetR(counts() As Integer, ws As Tp, os As Tp) As Tp
        ' a - b
        '     |
        ' c - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For b As Integer = 1 To 4
                If b = a OrElse b = os.Item2 Then Continue For
                If counts(b Or ws.Item2) = 0 Then Continue For
                For c As Integer = 1 To 4
                    If c = os.Item3 Then Continue For
                    If counts(c Or ws.Item3) = 0 Then Continue For
                    If c = a AndAlso ws.Item1 = ws.Item3 AndAlso counts(c Or ws.Item3) < 2 Then Continue For
                    For d As Integer = 1 To 4
                        If d = b OrElse d = c OrElse d = os.item4 Then Continue For
                        If counts(d Or ws.Item4) = 0 Then Continue For
                        SearchQuartetR = New Tp(a Or ws.Item1, b Or ws.Item2, c Or ws.Item3, d Or ws.Item4)
                        Exit Function
                    Next d
                Next c
            Next b
        Next a
        SearchQuartetR = Nothing
    End Function
    
    Private Function SearchQuartetL(counts() As Integer, ws As Tp, os As Tp) As Tp
        ' a - b
        ' |   
        ' c - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For b As Integer = 1 To 4
                If b = a OrElse b = os.Item2 Then Continue For
                If counts(b Or ws.Item2) = 0 Then Continue For
                For c As Integer = 1 To 4
                    If c = a OrElse c = os.Item3 Then Continue For
                    If counts(c Or ws.Item3) = 0 Then Continue For
                    For d As Integer = 1 To 4
                        If d = c OrElse d = os.item4 Then Continue For
                        If counts(d Or ws.Item4) = 0 Then Continue For
                        If d = b AndAlso ws.Item2 = ws.Item4 AndAlso counts(d Or ws.Item4) < 2 Then Continue For
                        SearchQuartetL = New Tp(a Or ws.Item1, b Or ws.Item2, c Or ws.Item3, d Or ws.Item4)
                        Exit Function
                    Next d
                Next c
            Next b
        Next a
        SearchQuartetL = Nothing
    End Function
    
    Private Function SearchSextetR(counts() As Integer, ws As Tp, os As Tp) As Tp6
        ' a - x - b
        '         |
        ' c - y - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For x As Integer = 1 To 4
                If x = a Then Continue For
                If counts(x Or (4 << 3)) = 0 Then Continue For
                For b As Integer = 1 To 4
                    If b = x OrElse b = os.Item2 Then Continue For
                    If counts(b Or ws.Item2) = 0 Then Continue For
                    For c As Integer = 1 To 4
                        If c = os.Item3 Then Continue For
                        If counts(c Or ws.Item3) = 0 Then Continue For
                        If c = a AndAlso ws.Item3 = ws.Item1 AndAlso counts(c Or ws.Item3) < 2 Then Continue For
                        If c = x AndAlso ws.Item3 = (4 << 3) AndAlso counts(c Or ws.Item3) < 2 Then Continue For
                        For y As Integer = 1 To 4
                            If y = c Then Continue For
                            If counts(y Or (4 << 3)) = 0 Then Continue For
                            If y = x AndAlso counts(y Or (4 << 3)) < 2 Then Continue For
                            If y = a AndAlso (4 << 3) = ws.Item1 AndAlso counts(y Or (4 << 3)) < 2 Then Continue For
                            For d As Integer = 1 To 4
                                If d = b OrElse d = y OrElse d = os.item4 Then Continue For
                                If counts(d Or ws.Item4) = 0 Then Continue For
                                SearchSextetR = New Tp6(a Or ws.Item1, b Or ws.Item2, _
                                    c Or ws.Item3, d Or ws.Item4, x Or (4 << 3), y Or (4 << 3))
                                Exit Function
                            Next d
                        Next y
                    Next c
                Next b
            Next x
        Next a
        SearchSextetR = Nothing
    End Function

    Private Function SearchSextetL(counts() As Integer, ws As Tp, os As Tp) As Tp6
        ' a - x - b
        ' |
        ' c - y - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For x As Integer = 1 To 4
                If x = a Then Continue For
                If counts(x Or (4 << 3)) = 0 Then Continue For
                For b As Integer = 1 To 4
                    If b = x OrElse b = os.Item2 Then Continue For
                    If counts(b Or ws.Item2) = 0 Then Continue For
                    For c As Integer = 1 To 4
                        If c = a OrElse c = os.Item3 Then Continue For
                        If counts(c Or ws.Item3) = 0 Then Continue For
                        For y As Integer = 1 To 4
                            If y = c Then Continue For
                            If counts(y Or (4 << 3)) = 0 Then Continue For
                            If y = x AndAlso counts(y Or (4 << 3)) < 2 Then Continue For
                            If y = b AndAlso (4 << 3) = ws.Item2 AndAlso counts(y Or (4 << 3)) < 2 Then Continue For
                            For d As Integer = 1 To 4
                                If d = y OrElse d = os.item4 Then Continue For
                                If counts(d Or ws.Item4) = 0 Then Continue For
                                If d = b AndAlso ws.Item4 = ws.Item2 AndAlso counts(d Or ws.Item4) < 2 Then Continue For
                                If d = x AndAlso ws.Item4 = (4 << 3) AndAlso counts(d Or ws.Item4) < 2 Then Continue For
                                SearchSextetL = New Tp6(a Or ws.Item1, b Or ws.Item2, _
                                    c Or ws.Item3, d Or ws.Item4, x Or (4 << 3), y Or (4 << 3))
                                Exit Function
                            Next d
                        Next y
                    Next c
                Next b
            Next x
        Next a
        SearchSextetL = Nothing
    End Function
    
    Private Function SearchSextetT(counts() As Integer, ws As Tp, os As Tp) As Tp6
        ' a - b
        ' |   |
        ' x   y
        ' |   |
        ' c   d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For x As Integer = 1 To 4
                If x = a Then Continue For
                If counts(x Or (5 << 3)) = 0 Then Continue For
                For b As Integer = 1 To 4
                    If b = a OrElse b = os.Item2 Then Continue For
                    If counts(b Or ws.Item2) = 0 Then Continue For
                    For c As Integer = 1 To 4
                        If c = x OrElse c = os.Item3 Then Continue For
                        If counts(c Or ws.Item3) = 0 Then Continue For
                        For y As Integer = 1 To 4
                            If y = b Then Continue For
                            If counts(y Or (5 << 3)) = 0 Then Continue For
                            If y = x AndAlso counts(y Or (5 << 3)) < 2 Then Continue For
                            If y = c AndAlso (5 << 3) = ws.Item3 AndAlso counts(y Or (5 << 3)) < 2 Then Continue For
                            For d As Integer = 1 To 4
                                If d = y OrElse d = os.item4 Then Continue For
                                If counts(d Or ws.Item4) = 0 Then Continue For
                                If d = c AndAlso ws.Item4 = ws.Item3 AndAlso counts(d Or ws.Item4) < 2 Then Continue For
                                If d = x AndAlso ws.Item4 = (5 << 3) AndAlso counts(d Or ws.Item4) < 2 Then Continue For
                                SearchSextetT = New Tp6(a Or ws.Item1, b Or ws.Item2, _
                                    c Or ws.Item3, d Or ws.Item4, x Or (5 << 3), y Or (5 << 3))
                                Exit Function
                            Next d
                        Next y
                    Next c
                Next b
            Next x
        Next a
        SearchSextetT = Nothing
    End Function
    
    Private Function SearchSextetB(counts() As Integer, ws As Tp, os As Tp) As Tp6
        ' a   b
        ' |   |
        ' x   y
        ' |   |
        ' c - d
        For a As Integer = 1 To 4
            If a = os.Item1 Then Continue For
            If counts(a Or ws.Item1) = 0 Then Continue For
            For x As Integer = 1 To 4
                If x = a Then Continue For
                If counts(x Or (5 << 3)) = 0 Then Continue For
                For b As Integer = 1 To 4
                    If b = os.Item2 Then Continue For
                    If counts(b Or ws.Item2) = 0 Then Continue For
                    If b = a AndAlso ws.Item2 = ws.Item1 AndAlso counts(b Or ws.Item2) < 2 Then Continue For
                    If b = x AndAlso ws.Item2 = (5 << 3) AndAlso counts(b Or ws.Item2) < 2 Then Continue For
                    For c As Integer = 1 To 4
                        If c = x OrElse c = os.Item3 Then Continue For
                        If counts(c Or ws.Item3) = 0 Then Continue For
                        For y As Integer = 1 To 4
                            If y = b Then Continue For
                            If counts(y Or (5 << 3)) = 0 Then Continue For
                            If y = x AndAlso counts(y Or (5 << 3)) < 2 Then Continue For
                            If y = a AndAlso (5 << 3) = ws.Item1 AndAlso counts(y Or (5 << 3)) < 2 Then Continue For
                            For d As Integer = 1 To 4
                                If d = c OrElse d = y OrElse d = os.item4 Then Continue For
                                If counts(d Or ws.Item4) = 0 Then Continue For
                                SearchSextetB = New Tp6(a Or ws.Item1, b Or ws.Item2, _
                                    c Or ws.Item3, d Or ws.Item4, x Or (5 << 3), y Or (5 << 3))
                                Exit Function
                            Next d
                        Next y
                    Next c
                Next b
            Next x
        Next a
        SearchSextetB = Nothing
    End Function
    
    Private Function ExpandCorner(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ExpandCorner = False
        
        Dim t1 As Integer = tmp(y    , x    )
        Dim t2 As Integer = tmp(y    , x + 1)
        Dim t3 As Integer = tmp(y + 1, x    )
        Dim t4 As Integer = tmp(y + 1, x + 1)
        ' 1 2
        ' 3 4
        
        If t1 < 0 Then 
            ' .┌      ┌─
            ' ┌┘  =>  │.
            If t2 >> 3 <> 0 OrElse t3 >> 3 <> 0 OrElse t4 >> 3 <> 2 Then Exit Function
            Dim c2 As Integer = t2 And 7, c3 As Integer = t3 And 7
            If counts(c2 Or (4 << 3)) = 0 OrElse counts(c3 Or (5 << 3)) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c2 OrElse i = c3 Then Continue For
                If counts(i Or (0 << 3)) > selc Then
                    seli = i: selc = counts(i Or (0 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t2) += 1
                counts(t3) += 1
                counts(t4) += 1
                tmp(y    , x    ) = seli Or (0 << 3)
                tmp(y    , x + 1) = c2   Or (4 << 3)
                tmp(y + 1, x    ) = c3   Or (5 << 3)
                tmp(y + 1, x + 1) = -1
                counts(seli Or (0 << 3)) -= 1
                counts(c2   Or (4 << 3)) -= 1
                counts(c3   Or (5 << 3)) -= 1
                ExpandCorner = True
            End If
            Exit Function
        End If
        
        If t2 < 0 Then
            ' ┐.      ─┐
            ' └┐  =>  .│
            If t1 >> 3 <> 1 OrElse t3 >> 3 <> 3 OrElse t4 >> 3 <> 1 Then Exit Function
            Dim c1 As Integer = t1 And 7, c4 As Integer = t4 And 7
            If counts(c1 Or (4 << 3)) = 0 OrElse counts(c4 Or (5 << 3)) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c1 OrElse i = c4 Then Continue For
                If counts(i Or (1 << 3)) > selc Then
                    seli = i: selc = counts(i Or (1 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t1) += 1
                counts(t3) += 1
                counts(t4) += 1
                tmp(y    , x    ) = c1   Or (4 << 3)
                tmp(y    , x + 1) = seli Or (1 << 3)
                tmp(y + 1, x    ) = -1
                tmp(y + 1, x + 1) = c4   Or (5 << 3)
                counts(seli Or (1 << 3)) -= 1
                counts(c1   Or (4 << 3)) -= 1
                counts(c4   Or (5 << 3)) -= 1
                ExpandCorner = True
            End If
            Exit Function
        End If
        
        If t3 < 0 Then
            ' └┐      │.
            ' .└  =>  └─
            If t1 >> 3 <> 3 OrElse t2 >> 3 <> 1 OrElse t4 >> 3 <> 3 Then Exit Function
            Dim c1 As Integer = t1 And 7, c4 As Integer = t4 And 7
            If counts(c1 Or (5 << 3)) = 0 OrElse counts(c4 Or (4 << 3)) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c1 OrElse i = c4 Then Continue For
                If counts(i Or (3 << 3)) > selc Then
                    seli = i: selc = counts(i Or (3 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t1) += 1
                counts(t2) += 1
                counts(t4) += 1
                tmp(y    , x    ) = c1   Or (5 << 3)
                tmp(y    , x + 1) = -1
                tmp(y + 1, x    ) = seli Or (3 << 3)
                tmp(y + 1, x + 1) = c4   Or (4 << 3)
                counts(seli Or (3 << 3)) -= 1
                counts(c1   Or (5 << 3)) -= 1
                counts(c4   Or (4 << 3)) -= 1
                ExpandCorner = True
            End If
            Exit Function
        End If
        
        If t4 < 0 Then
            ' ┌┘      .│
            ' ┘.  =>  ─┘
            If t1 >> 3 <> 0 OrElse t2 >> 3 <> 2 OrElse t3 >> 3 <> 2 Then Exit Function
            Dim c2 As Integer = t2 And 7, c3 As Integer = t3 And 7
            If counts(c2 Or (5 << 3)) = 0 OrElse counts(c3 Or (4 << 3)) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c2 OrElse i = c3 Then Continue For
                If counts(i Or (2 << 3)) > selc Then
                    seli = i: selc = counts(i Or (2 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t1) += 1
                counts(t2) += 1
                counts(t3) += 1
                tmp(y    , x    ) = -1
                tmp(y    , x + 1) = c2   Or (5 << 3)
                tmp(y + 1, x    ) = c3   Or (4 << 3)
                tmp(y + 1, x + 1) = seli Or (2 << 3)
                counts(seli Or (2 << 3)) -= 1
                counts(c2   Or (5 << 3)) -= 1
                counts(c3   Or (4 << 3)) -= 1
                ExpandCorner = True
            End If
            Exit Function
        End If
        
    End Function
    
    Private Function SwapCorner(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        ' ┌┘     .│
        ' │.  => ┌┘
        SwapCorner = False
        
        Dim t1 As Integer = tmp(y    , x    )
        Dim t2 As Integer = tmp(y    , x + 1)
        Dim t3 As Integer = tmp(y + 1, x    )
        Dim t4 As Integer = tmp(y + 1, x + 1)
        ' 1 2
        ' 3 4
        
        If t1 < 0 Then
            If t4 >> 3 <> 2 Then Exit Function
            Dim wr2, wr3 As Integer
            
            If t2 >> 3 = 5 AndAlso t3 >> 3 = 0 Then
                ' .│    ┌┘
                ' ┌┘ => │.
                wr2 = 2 << 3: wr3 = 5 << 3
            ElseIf t2 >> 3 = 0 AndAlso t3 >> 3 = 4 Then
                ' .┌    ┌─
                ' ─┘ => ┘.
                wr2 = 4 << 3: wr3 = 2 << 3
            Else
                Exit Function
            End If
                
            Dim c2 As Integer = t2 And 7, c3 As Integer = t3 And 7
            If counts(c2 Or wr2) = 0 OrElse counts(c3 Or wr3) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c2 OrElse i = c3 Then Continue For
                If counts(i Or (0 << 3)) > selc Then
                    seli = i: selc = counts(i Or (0 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t2) += 1
                counts(t3) += 1
                counts(t4) += 1
                tmp(y    , x    ) = seli Or (0 << 3)
                tmp(y    , x + 1) = c2   Or wr2
                tmp(y + 1, x    ) = c3   Or wr3
                tmp(y + 1, x + 1) = -1
                counts(seli Or (0 << 3)) -= 1
                counts(c2   Or wr2     ) -= 1
                counts(c3   Or wr3     ) -= 1
                SwapCorner = True
                ' Console.Error.Write("SwapA!")
            End If
                
            Exit Function
        End If
        
        If t2 < 0 Then
            If t3 >> 3 <> 3 Then Exit Function
            Dim wr1, wr4 As Integer
            
            If t1 >> 3 = 5 AndAlso t4 >> 3 = 1 Then
                ' │.    └┐
                ' └┐ => .│
                wr1 = 3 << 3: wr4 = 5 << 3
            ElseIf t1 >> 3 = 1 AndAlso t4 >> 3 = 4 Then
                ' ┐.    ─┐
                ' └─ => .└
                wr1 = 4 << 3: wr4 = 3 << 3
            Else
                Exit Function
            End If
                
            Dim c1 As Integer = t1 And 7, c4 As Integer = t4 And 7
            If counts(c1 Or wr1) = 0 OrElse counts(c4 Or wr4) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c1 OrElse i = c4 Then Continue For
                If counts(i Or (1 << 3)) > selc Then
                    seli = i: selc = counts(i Or (1 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t1) += 1
                counts(t3) += 1
                counts(t4) += 1
                tmp(y    , x    ) = c1   Or wr1
                tmp(y    , x + 1) = seli Or (1 << 3)
                tmp(y + 1, x    ) = -1
                tmp(y + 1, x + 1) = c4   Or wr4
                counts(seli Or (1 << 3)) -= 1
                counts(c1   Or wr1     ) -= 1
                counts(c4   Or wr4     ) -= 1
                SwapCorner = True
                ' Console.Error.Write("SwapB!")
            End If
                
            Exit Function
        End If
        
        If t3 < 0 Then
            If t2 >> 3 <> 1 Then Exit Function
            Dim wr1, wr4 As Integer
            
            If t1 >> 3 = 3 AndAlso t4 >> 3 = 5 Then
                ' └┐    │.
                ' .│ => └┐
                wr1 = 5 << 3: wr4 = 1 << 3
            ElseIf t1 >> 3 = 4 AndAlso t4 >> 3 = 3 Then
                ' ─┐    ┐.    
                ' .└ => └─
                wr1 = 1 << 3: wr4 = 4 << 3
            Else
                Exit Function
            End If
                
            Dim c1 As Integer = t1 And 7, c4 As Integer = t4 And 7
            If counts(c1 Or wr1) = 0 OrElse counts(c4 Or wr4) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c1 OrElse i = c4 Then Continue For
                If counts(i Or (3 << 3)) > selc Then
                    seli = i: selc = counts(i Or (3 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t1) += 1
                counts(t2) += 1
                counts(t4) += 1
                tmp(y    , x    ) = c1   Or wr1
                tmp(y    , x + 1) = -1
                tmp(y + 1, x    ) = seli Or (3 << 3)
                tmp(y + 1, x + 1) = c4   Or wr4
                counts(seli Or (3 << 3)) -= 1
                counts(c1   Or wr1     ) -= 1
                counts(c4   Or wr4     ) -= 1
                SwapCorner = True
                ' Console.Error.Write("SwapC!")
            End If
                
            Exit Function
        End If
        
        If t4 < 0 Then
            If t1 >> 3 <> 0 Then Exit Function
            Dim wr2, wr3 As Integer
            
            If t2 >> 3 = 2 AndAlso t3 >> 3 = 5 Then
                '  ┌┘    .│    
                '  │. => ┌┘
                wr2 = 5 << 3: wr3 = 0 << 3
            ElseIf t2 >> 3 = 4 AndAlso t3 >> 3 = 2 Then
                ' ┌─    .┌    
                ' ┘. => ─┘
                wr2 = 0 << 3: wr3 = 4 << 3
            Else
                Exit Function
            End If
                
            Dim c2 As Integer = t2 And 7, c3 As Integer = t3 And 7
            If counts(c2 Or wr2) = 0 OrElse counts(c3 Or wr3) = 0 Then Exit Function
            Dim seli As Integer = 0, selc As Integer = 0
            For i As Integer = 1 To 4
                If i = c2 OrElse i = c3 Then Continue For
                If counts(i Or (2 << 3)) > selc Then
                    seli = i: selc = counts(i Or (2 << 3))
                End If
            Next i
            If seli > 0 Then
                counts(t1) += 1
                counts(t2) += 1
                counts(t3) += 1
                tmp(y    , x    ) = -1
                tmp(y    , x + 1) = c2   Or wr2
                tmp(y + 1, x    ) = c3   Or wr3
                tmp(y + 1, x + 1) = seli Or (2 << 3)
                counts(seli Or (2 << 3)) -= 1
                counts(c2   Or wr2     ) -= 1
                counts(c3   Or wr3     ) -= 1
                SwapCorner = True
                ' Console.Error.Write("SwapD!")
            End If
                
            Exit Function
        End If
        
    End Function
    
    Private Function Change(tmp(,) As Integer, counts() As Integer, x As Integer, y As Integer) As Boolean
        Dim t1 As Integer = tmp(y, x)
        Dim w1 As Integer = t1 >> 3
        Dim c1 As Integer = t1 And 7
        Dim c2, c3 As Integer
        Select Case w1
        Case 0: c2 = tmp(y, x + 1) And 7: c3 = tmp(y + 1, x) And 7
        Case 1: c2 = tmp(y, x - 1) And 7: c3 = tmp(y + 1, x) And 7
        Case 2: c2 = tmp(y, x - 1) And 7: c3 = tmp(y - 1, x) And 7
        Case 3: c2 = tmp(y, x + 1) And 7: c3 = tmp(y - 1, x) And 7
        Case 4: c2 = tmp(y, x + 1) And 7: c3 = tmp(y, x - 1) And 7
        Case 5: c2 = tmp(y - 1, x) And 7: c3 = tmp(y + 1, x) And 7
        End Select
        Change = False
        For i As Integer = 1 To 4
            If i = c1 OrElse i = c2 OrElse i = c3 Then Continue For
            Dim t2 As Integer = i Or (w1 << 3)
            If counts(t2) = 0 Then Continue For
            tmp(y, x) = t2
            counts(t1) += 1
            counts(t2) -= 1
            Change = True
            Exit For
        Next i
    End Function
    
    Private Function idea4_partial(H As Integer, W As Integer, colors As Integer, tmp(,) As  Integer, counts() As Integer, time0 As Integer) As Boolean
        
        ' Console.Error.WriteLine("i4p")
        
        Dim swapflag(H - 1, W - 1) As Boolean
        
        Do
            Dim time1 As Integer = Environment.TickCount
            If time1 > time0 Then
                idea4_partial = True
                Exit Function
            End If
            
            Dim nochange As Boolean = True
            
            For y As Integer = 0 To H - 1
                For x As Integer = 0 To W - 1
                    If tmp(y, x) < 0 Then Continue For
                    If x < W - 1 Then
                        If y > 0 AndAlso ExtendToTop(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                        If y < H - 1 AndAlso ExtendToBottom(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                    End If
                    If y < H - 1 Then
                        If x < W - 1 AndAlso ExtendToRight(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                        If x > 0 AndAlso ExtendToLeft(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                    End If
                Next x
            Next y

            If nochange Then
                For y As Integer = 0 To H - 1
                    For x As Integer = 0 To W - 1
                        If tmp(y, x) < 0 Then Continue For
                        If x < W - 1 Then
                            If y > 1 AndAlso ExtendToTop2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                            If y < H - 2 AndAlso ExtendToBottom2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                        End If
                        If y < H - 1 Then
                            If x < W - 2 AndAlso ExtendToRight2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                            If x > 1 AndAlso ExtendToLeft2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                        End If
                    Next x
                Next y
            End If
            
            If nochange Then
                For y As Integer = 0 To H - 2
                    For x As Integer = 0 To W - 2
                        If ExpandCorner(tmp, counts, x, y) Then
                            nochange = False
                            ' Console.Error.Write("Expand!")
                        End If
                    Next x
                Next y                
            End If

            If nochange Then
                For y As Integer = 0 To H - 2
                    For x As Integer = 0 To W - 2
                        If swapflag(y, x) Then Continue For
                        If SwapCorner(tmp, counts, x, y) Then
                            nochange = False
                            swapflag(y, x) = True
                            ' Console.Error.Write("Swap!")
                            GoTo EscapeSwap
                        End If
                    Next x
                Next y
EscapeSwap:
            End If
            
            If nochange Then
                For y As Integer = 0 To H - 1
                    For x As Integer = 0 To W - 1
                        If tmp(y, x) < 0 Then Continue For
                        If Change(tmp, counts, x, y) Then
                            nochange = False
                        End If
                    Next x
                Next y
                
                If nochange Then Exit Do
            End If
        Loop
        
        idea4_partial = False
    End Function
    
    Private Sub idea4(H As Integer, W As Integer, tmp(,) As Integer, counts() As Integer, time0 As Integer)
        
        Dim cols As Tp = SearchQuartet(counts, New Tp(0 << 3, 1 << 3, 3 << 3, 2 << 3), New Tp(0, 0, 0, 0))
        If cols IsNot Nothing Then
            Dim x As Integer = W \ 2
            Dim y As Integer = H \ 2
            PlaceQuartet(tmp, counts, x, y, cols)
        End If
        
        Dim swapflag(H - 1, W - 1) As Boolean

        Do
            Dim time1 As Integer = Environment.TickCount
            If time1 > time0 Then Exit Do
            
            Dim nochange As Boolean = True
            
            For y As Integer = 0 To H - 1
                For x As Integer = 0 To W - 1
                    If tmp(y, x) < 0 Then Continue For
                    If x < W - 1 Then
                        If y > 0 AndAlso ExtendToTop(tmp, counts, x, y) Then
                            nochange = False
                             Continue For
                        End If
                        If y < H - 1 AndAlso ExtendToBottom(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                    End If
                    If y < H - 1 Then
                        If x < W - 1 AndAlso ExtendToRight(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                        If x > 0 AndAlso ExtendToLeft(tmp, counts, x, y) Then
                            nochange = False
                            Continue For
                        End If
                    End If
                Next x
            Next y
            
            If nochange Then
                For y As Integer = 0 To H - 1
                    For x As Integer = 0 To W - 1
                        If tmp(y, x) < 0 Then Continue For
                        If x < W - 1 Then
                            If y > 1 AndAlso ExtendToTop2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                            If y < H - 2 AndAlso ExtendToBottom2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                        End If
                        If y < H - 1 Then
                            If x < W - 2 AndAlso ExtendToRight2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                            If x > 1 AndAlso ExtendToLeft2(tmp, counts, x, y) Then
                                nochange = False
                                Continue For
                            End If
                        End If
                    Next x
                Next y
            End If
            
            If nochange Then
                For y As Integer = 0 To H - 2
                    For x As Integer = 0 To W - 2
                        If ExpandCorner(tmp, counts, x, y) Then nochange = False
                    Next x
                Next y                
            End If

            If nochange Then
                For y As Integer = 0 To H - 2
                    For x As Integer = 0 To W - 2
                        If swapflag(y, x) Then Continue For
                        If SwapCorner(tmp, counts, x, y) Then
                            nochange = False
                            swapflag(y, x) = True
                            ' Console.Error.Write("Swap!")
                            GoTo EscapeSwap
                        End If
                    Next x
                Next y
EscapeSwap:
            End If
            
            If nochange Then
                For y As Integer = 0 To H - 1
                    For x As Integer = 0 To W - 1
                        If tmp(y, x) < 0 Then Continue For
                        If Change(tmp, counts, x, y) Then
                            nochange = False
                        End If
                    Next x
                Next y
                
                If nochange Then
                    Dim f As Boolean = True
                    For y As Integer = 0 To H - 2
                        For x As Integer = 0 To W - 2
                            If swapflag(y, x) Then
                                swapflag(y, x) = False
                                f = False
                            End If
                        Next x
                    Next y
                    If f Then Exit Do
                End If
            End If
        Loop
        
    End Sub
    
    Private Function TryHilbert(H As Integer, W As Integer, colors As Integer, _
            tiles() As Stack(Of Integer), wires() As Integer, _
            counts() As Integer, gap As Integer, mins() As Integer, _
            time0 As Integer, time9 As Integer) As Result
        Dim limit As Integer = H * W
        If colors = 2 Then
            For i = 0 To 3
                limit = Math.Min(limit, mins(i) * 2 \ 3)
            Next i
            FOr i = 4 To 5
                limit = Math.Min(limit, mins(i))
            Next i
        Else
            For i = 0 To 3
                limit = Math.Min(limit, wires(i) \ 3)
            Next i
            FOr i = 4 To 5
                limit = Math.Min(limit, wires(i) \ 2)
            Next i
        End If
        
        Dim tmp(H - 1, W - 1) As Integer
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                tmp(y, x) = -1
            Next x
        Next y
        
        Dim ks As Integer = 0
        Dim ke As Integer = 50
        Dim kp As Integer = 1
        If colors = 2 Then
            ks = 0
            ke = gap * 30
            kp = If(gap > 20, gap \ 10, 1)
        End If
        
        Dim found As Boolean = False
        
        For k As Integer = ks To ke Step kp
            Dim time1 As Integer = Environment.TickCount
            If time1 > time0 Then Exit For
            Dim tmp_tmp(H - 1, W - 1) As Integer
            For y As Integer = 0 To H - 1
                For x As Integer = 0 To W - 1
                    tmp_tmp(y, x) = -1
                Next x
            Next y
            Dim tmp_counts(50) As Integer
            Dim tmp_wires(50) As Integer
            For j As Integer = 0 To 5
                For i As Integer = 1 To 4
                    Dim t As Integer = i Or (j << 3)
                    tmp_counts(t) = tiles(t).Count
                    tmp_wires(j) += tiles(t).Count
                Next i
            Next j
            If Search(H, W, colors, tmp_tmp, tmp_wires, tmp_counts, limit - k) Then
                tmp = tmp_tmp
                counts = tmp_counts
                found = True
                Exit For
            End If
        Next k
        
        If Not found Then
            TryHilbert = Nothing
            Exit Function
        Else
            Dim dh As Integer = 0
            Dim dw As Integer = 0
            Dim sc As Integer = CalcScore(counts)
            For i As Integer = 1 To H - 1
                Dim cm As Boolean = False
                For j As Integer = 0 To W - 1
                    If tmp(H - i, j) >= 0 Then
                        cm = True
                        Exit For
                    End If
                Next j
                If cm Then Exit For
                dh = i
            Next i
            For j As Integer = 1 To W - 1
                Dim cm As Boolean = False
                For i As Integer = 0 To H - 1
                    If tmp(i, W - j) >= 0 Then
                        cm = True
                        Exit For
                    End IF
                Next i
                If cm Then Exit For
                dw = j
            Next j
            
            Dim ctime As Integer = Environment.TickCount
            Dim dttime As Integer = Math.Max(200, (time9 - ctime) \ ((dh + 1) * (dw + 1)))
            Dim ttime As Integer = Math.Min(ctime + dttime, time9)
            
            Dim sel_tmp(,) As Integer = tmp
            Dim sel_counts() As Integer = counts
            For dy As Integer = 0 To dh
                For dx As Integer = 0 To dw
                    Dim tmp_counts(50) As Integer
                    Array.Copy(counts, tmp_counts, tmp_counts.Length)
                    Dim tmp_tmp(H - 1, W - 1) As Integer
                    For i As Integer = 0 To H - 1
                        Dim y As Integer = i - dy
                        For j As Integer = 0 To W - 1
                            Dim x As Integer = j - dx
                            If x < 0 OrElse y < 0 OrElse x >= W OrElse y >= H Then
                                tmp_tmp(i, j) = -1
                            Else
                                tmp_tmp(i, j) = tmp(y, x)
                            End If
                        Next j
                    Next i
                    idea4_partial(H, W, colors, tmp_tmp, tmp_counts, ttime)
                    Dim tsc As Integer = CalcScore(tmp_counts)
                    If tsc < sc Then
                        sc = tsc
                        sel_tmp = tmp_tmp
                        sel_counts = tmp_counts
                    End IF
                    If ttime = time9 Then GoTo OuterLoop
                    ttime = Math.Min(ttime + dttime, time9)
                Next dx
            Next dy
OuterLoop:
            tmp = sel_tmp
            counts = sel_counts
        End If
        
        TryHilbert = New Result(tmp, counts)
    End Function

    Private Function CalcScore(counts() As Integer) As Integer
        Dim score As Integer = 0
        For j As Integer = 0 To 5
            For i As Integer = 1 To 4
                score += counts(i Or (j << 3))
            Next i
        Next j
        CalcScore = score
    End Function


    Private Function Search(H As Integer, W As Integer, colors As Integer, tmp(,) As Integer, wires() As Integer, counts() As Integer, limit As Integer) As Boolean
        Search = False

        Dim tmpH As Integer = (H - 4) \ 4
        Dim tmpW As Integer = (W - 8) \ 4
        
        Dim startP As New Pos(0, 3)
        Dim goalP  As New Pos((tmpW + 1) * 4 + 3, 3)
        Dim startF As Integer = 2
        
        Dim hls As Pos = HeadLines(tmp, wires, tmpW, limit)
        
        If hls IsNot Nothing Then
            If hls.Item1 < 0 Then
                Exit Function
            End If
            goalP = New Pos(hls.Item2 * 4 + 3, 3)
            
        Else
        
            If tmpW = 0 Then
                Dim p As Pos = DoubleLines(tmp, wires, tmpH - 1, limit)
                If p IsNot Nothing Then
                    startP = New Pos(0, p.Item1)
                    startF = 2
                    goalP = New Pos(7, p.Item2)
                Else
                    goalP = New Pos(7, tmpH * 4 - 1)
                    Dim r As XYZ = UnderLines(tmp, wires, tmpH * 4, tmpW, limit)
                    startP = New Pos(r.Item1, r.Item2)
                    startF = r.Item3
                End If
            ElseIf tmpH = 1 Then
                Dim r As XYZ = UnderLines(tmp, wires, 4, tmpW, limit)
                startP = New Pos(r.Item1, r.Item2)
                startF = r.Item3
            ElseIf tmpH = 2 Then
                goalP = ZigZag(tmp, wires, 4, tmpW, limit)
            ElseIf tmpH Mod 2 = 0 Then
                Dim r As XYZ = TwoLines(tmp, wires, tmpH - 2, tmpW, limit)
                If r IsNot Nothing Then
                    startP = New Pos(0, r.Item3): startF = 2
                    goalP = New Pos(r.Item1, r.Item2)
                Else
                    Dim y As Integer = (tmpH - 1) * 4 
                    startP = New Pos(0, y - 1): startF = 2
                    goalP = ZigZag(tmp, wires, y, tmpW, limit)
                End If
            Else
                Dim r As XYZ = TwoLines(tmp, wires, tmpH - 1, tmpW, limit)
                If r IsNot Nothing Then
                    startP = New Pos(0, r.Item3): startF = 2
                    goalP = New Pos(r.Item1, r.Item2)
                Else
                    Dim y As Integer = tmpH * 4 
                    goalP = New Pos(tmpW * 4 + 3, y - 1)
                    r = UnderLines(tmp, wires, y, tmpW, limit)
                    startP = New Pos(r.Item1, r.Item2)
                    startF = r.Item3
                End If
            End If
            
        End If
        
        InitDfs()
        If Not Dfs(tmp, wires, startF, startP, goalP, New Bounds(0, 0, W - 1, H - 1), 0) Then
            Console.Error.WriteLine("failure Dfs")
            'PrintTable(H, W, tmp, "failure Dfs")
            Exit Function
        End If
        
        If Not Place(New Pos(W, H), tmp, counts, New Pos(0, 0), colors) Then
            Console.Error.WriteLine("failure Place")
            'PrintTable(H, W, tmp, "failure Place")
            Exit Function
        End If
        
        Search = True
    End Function
    
    Private Function HeadLines(tmp(,) As Integer, wires() As Integer, tmpW As Integer, ByRef limit As Integer) As Pos
        If Not HilbertS(tmp, wires, New Pos(0, 0), New Pos(0, 4), limit) Then
            HeadLines = New Pos(-1, -1)
            Exit Function
        End If
        Dim horiP As New Pos(4, 4)
        For i As Integer = 1 To tmpW
            If Not HilbertS(tmp, wires, New Pos(i * 4, 0), horiP, limit) Then
                HeadLines = New Pos(0, i - 1)
                Exit Function
            End If
        Next i
        If Not HilbertS(tmp, wires, New Pos((tmpW + 1) * 4, 0), New Pos(4, 1), limit) Then
            HeadLines = New Pos(0, tmpW)
            Exit Function
        End If
        HeadLines = Nothing
    End Function
    
    Private Function DoubleLines(tmp(,) As Integer, wires() As Integer, tmpH As Integer, ByRef limit As Integer) As Pos
        Dim pp As New Pos(5, 5)
        For i As Integer = 1 To tmpH
            If Not HilbertW(tmp, wires, New Pos(0, i * 4), pp, limit) Then
                DoubleLines = New Pos(i * 4 - 1, i * 4 - 1)
                Exit Function
            End If
            If Not HilbertE(tmp, wires, New Pos(4, i * 4), pp, limit) Then
                DoubleLines = New Pos(i * 4 + 3, i * 4 - 1)
                Exit Function
            End If
        Next i
        DoubleLines = Nothing
    End Function
    
    Private Function UnderLines(tmp(,) As Integer, wires() As Integer, y0 As Integer, tmpW As Integer, ByRef limit As Integer) As XYZ
        If Not HilbertN(tmp, wires, New Pos(0, y0), New Pos(3, 4), limit) Then
            UnderLines = New XYZ(0, y0 - 1, 2): Exit Function
        End If
        Dim horiP As New Pos(4, 4)
        For i As Integer = 1 To tmpW
            If Not HilbertN(tmp, wires, New Pos(i * 4, y0), horiP, limit) Then
                UnderLines = New XYZ(i * 4 - 1, y0, 1): Exit Function
            End If
        Next i
        If Not HilbertN(tmp, wires, New Pos((tmpW + 1) * 4, y0), New Pos(4, 2), limit) Then
            UnderLines = New XYZ(tmpW * 4 + 3, y0, 1)
        Else
            UnderLines = New XYZ((tmpW + 1) * 4 + 3, y0, 0)
        End If
    End Function
    
    Private Function ZigZag(tmp(,) As Integer, wires() As Integer, y0 As Integer, tmpW As Integer, ByRef limit As Integer) As Pos
        If Not HilbertE(tmp, wires, New Pos((tmpW + 1) * 4, y0), New Pos(5, 5), limit) Then
            ZigZag = New Pos((tmpW + 1) * 4 + 3, y0 - 1): Exit Function
        End If
        If Not HilbertN(tmp, wires, New Pos((tmpW + 1) * 4, y0 + 4), New Pos(4, 2), limit) Then
            ZigZag = New Pos((tmpW + 1) * 4 + 3, y0 + 3): Exit Function
        End If
        Dim flip As Boolean = True
        Dim pN1 As New Pos(3, 4)
        Dim pW As New Pos(1, 5)
        Dim pE As New Pos(0, 5)
        Dim pN2 As New Pos(4, 2)
        For i As Integer = tmpW To 1 Step -1
            If flip Then
                If Not HilbertN(tmp, wires, New Pos(i * 4, y0 + 4), pN1, limit) Then
                    ZigZag = New Pos((i + 1) * 4, y0 + 4): Exit Function
                End If
                If Not HilbertW(tmp, wires, New Pos(i * 4, y0), pW, limit) Then
                    ZigZag = New Pos(i * 4, y0 + 4): Exit Function
                End If
            Else
                If Not HilbertE(tmp, wires, New Pos(i * 4, y0), pE, limit) Then
                    ZigZag = New Pos((i + 1) * 4, y0): Exit Function
                End If
                If Not HilbertN(tmp, wires, New Pos(i * 4, y0 + 4), pN2, limit) Then
                    ZigZag = New Pos(i * 4 + 3, y0 + 3): Exit Function
                End If
            End If
            flip = Not flip
        Next i
        If flip Then
            If Not HilbertN(tmp, wires, New Pos(0, y0 + 4), pN1, limit) Then
                ZigZag = New Pos(4, y0 + 4): Exit Function
            End If
            If Not HilbertW(tmp, wires, New Pos(0, y0), New Pos(5, 5), limit) Then
                ZigZag = New Pos(0, y0 + 4): Exit Function
            End If
        Else
            If Not HilbertN(tmp, wires, New Pos(0, y0), pN1, limit) Then
                ZigZag = New Pos(4, y0): Exit Function
            End If
        End If
        ZigZag = New Pos(0, y0)
    End Function
    
    Private Function TwoLines(tmp(,) As Integer, wires() As Integer, tmpH As Integer, tmpW As Integer, ByRef limit As Integer) As XYZ
        Dim flip As Boolean = True
        Dim pW As New Pos(5, 5)
        Dim pN1 As New Pos(4, 2)
        Dim pN2 As New Pos(4, 4)
        Dim pE1 As New Pos(0, 5)
        Dim pE2 As New Pos(5, 3)
        Dim pS1 As Pos = pN2
        Dim pS2 As new Pos(4, 1)
        For i As Integer = 1 To tmpH
            If flip Then
                If Not HilbertW(tmp, wires, New Pos(0, i * 4), pW, limit) Then
                    TwoLines = New XYZ((tmpW + 1) * 4 + 3, i * 4 - 1, i * 4 - 1)
                    Exit Function
                End If
                If Not HilbertN(tmp, wires, New Pos((tmpW + 1) * 4, i * 4), pN1, limit) Then
                    TwoLines = New XYZ((tmpW + 1) * 4 + 3, i * 4 - 1, i * 4 + 3)
                    Exit Function
                End If
                For j As Integer = tmpW To 2 Step -1
                    If Not HilbertN(tmp, wires, New Pos(j * 4, i * 4), pN2, limit) Then
                        TwoLines = New XYZ((j + 1) * 4, i * 4, i * 4 + 3)
                        Exit Function
                    End If
                Next j
                If Not HilbertE(tmp, wires, New Pos(4, i * 4), pE1, limit) Then
                    TwoLines = New XYZ(8, i * 4, i * 4 + 3)
                    Exit Function
                End If
            Else
                If Not HilbertW(tmp, wires, New Pos(0, i * 4), pW, limit) Then
                    TwoLines = New XYZ(7, i * 4 - 1, i * 4 - 1)
                    Exit Function
                End If
                If Not HilbertE(tmp, wires, New Pos(4, i * 4), pE2, limit) Then
                    TwoLines = New XYZ(7, i * 4 - 1, i * 4 + 3)
                    Exit Function
                End If
                For j As Integer = 2 To tmpW
                    If Not HilbertS(tmp, wires, New Pos(j * 4, i * 4), pS1, limit) Then
                        TwoLines = New XYZ(j * 4 - 1, i * 4 + 3, i * 4 + 3)
                        Exit Function
                    End If
                Next j
                If Not HilbertS(tmp, wires, New Pos((tmpW + 1) * 4, i * 4), pS2, limit) Then
                    TwoLines = New XYZ(tmpW * 4 + 3, i * 4 + 3, i * 4 + 3)
                    Exit Function
                End If
            End If
            flip = Not flip
        Next i
        TwoLines = Nothing
    End Function
    
    Private Function HilbertS(tmp(,) As Integer, wires() As Integer, p As Pos, f As Pos, ByRef limit As Integer) As Boolean
        If limit = 0 Then
            HilbertS = False: Exit Function
        End If
        Dim a As Integer = f.Item1: Dim b As Integer = f.Item2
        wires(a) -= 1: wires(b) -= 1
        If wires(0) < 3 OrElse wires(1) < 3 OrElse wires(2) < 3 OrElse wires(3) < 3 OrElse wires(5) < 2 Then
            wires(a) += 1: wires(b) += 1: HilbertS = False: Exit Function
        End If
        wires(0) -= 3: wires(1) -= 3: wires(2) -= 3: wires(3) -= 3: wires(5) -= 2
        Dim x As Integer = p.Item1: Dim y As Integer = p.Item2
        tmp(y+0, x+0) = 0: tmp(y+0, x+1) = 1: tmp(y+0, x+2) = 0: tmp(y+0, x+3) = 1
        tmp(y+1, x+0) = 5: tmp(y+1, x+1) = 3: tmp(y+1, x+2) = 2: tmp(y+1, x+3) = 5
        tmp(y+2, x+0) = 3: tmp(y+2, x+1) = 1: tmp(y+2, x+2) = 0: tmp(y+2, x+3) = 2
        tmp(y+3, x+0) = a: tmp(y+3, x+1) = 2: tmp(y+3, x+2) = 3: tmp(y+3, x+3) = b
        limit -= 1
        HilbertS = True
    End Function

    Private Function HilbertN(tmp(,) As Integer, wires() As Integer, p As Pos, f As Pos, ByRef limit As Integer) As Boolean
        If limit = 0 Then
            HilbertN = False: Exit Function
        End If
        Dim a As Integer = f.Item1: Dim b As Integer = f.Item2
        wires(a) -= 1: wires(b) -= 1
        If wires(0) < 3 OrElse wires(1) < 3 OrElse wires(2) < 3 OrElse wires(3) < 3 OrElse wires(5) < 2 Then
            wires(a) += 1: wires(b) += 1: HilbertN = False: Exit Function
        End If
        wires(0) -= 3: wires(1) -= 3: wires(2) -= 3: wires(3) -= 3: wires(5) -= 2
        Dim x As Integer = p.Item1: Dim y As Integer = p.Item2
        tmp(y+0, x+0) = a: tmp(y+0, x+1) = 1: tmp(y+0, x+2) = 0: tmp(y+0, x+3) = b
        tmp(y+1, x+0) = 0: tmp(y+1, x+1) = 2: tmp(y+1, x+2) = 3: tmp(y+1, x+3) = 1
        tmp(y+2, x+0) = 5: tmp(y+2, x+1) = 0: tmp(y+2, x+2) = 1: tmp(y+2, x+3) = 5
        tmp(y+3, x+0) = 3: tmp(y+3, x+1) = 2: tmp(y+3, x+2) = 3: tmp(y+3, x+3) = 2
        limit -= 1
        HilbertN = True
    End Function

    Private Function HilbertW(tmp(,) As Integer, wires() As Integer, p As Pos, f As Pos, ByRef limit As Integer) As Boolean
        If limit = 0 Then
            HilbertW = False: Exit Function
        End If
        Dim a As Integer = f.Item1: Dim b As Integer = f.Item2
        wires(a) -= 1: wires(b) -= 1
        If wires(0) < 3 OrElse wires(1) < 3 OrElse wires(2) < 3 OrElse wires(3) < 3 OrElse wires(4) < 2 Then
            wires(a) += 1: wires(b) += 1: HilbertW = False: Exit Function
        End If
        wires(0) -= 3: wires(1) -= 3: wires(2) -= 3: wires(3) -= 3: wires(4) -= 2
        Dim x As Integer = p.Item1: Dim y As Integer = p.Item2
        tmp(y+0, x+0) = a: tmp(y+0, x+1) = 0: tmp(y+0, x+2) = 4: tmp(y+0, x+3) = 1
        tmp(y+1, x+0) = 3: tmp(y+1, x+1) = 2: tmp(y+1, x+2) = 0: tmp(y+1, x+3) = 2
        tmp(y+2, x+0) = 0: tmp(y+2, x+1) = 1: tmp(y+2, x+2) = 3: tmp(y+2, x+3) = 1
        tmp(y+3, x+0) = b: tmp(y+3, x+1) = 3: tmp(y+3, x+2) = 4: tmp(y+3, x+3) = 2
        limit -= 1
        HilbertW = True
    End Function

    Private Function HilbertE(tmp(,) As Integer, wires() As Integer, p As Pos, f As Pos, ByRef limit As Integer) As Boolean
        If limit = 0 Then
            HilbertE = False: Exit Function
        End If
        Dim a As Integer = f.Item1: Dim b As Integer = f.Item2
        wires(a) -= 1: wires(b) -= 1
        If wires(0) < 3 OrElse wires(1) < 3 OrElse wires(2) < 3 OrElse wires(3) < 3 OrElse wires(4) < 2 Then
            wires(a) += 1: wires(b) += 1: HilbertE = False: Exit Function
        End If
        wires(0) -= 3: wires(1) -= 3: wires(2) -= 3: wires(3) -= 3: wires(4) -= 2
        Dim x As Integer = p.Item1: Dim y As Integer = p.Item2
        tmp(y+0, x+0) = 0: tmp(y+0, x+1) = 4: tmp(y+0, x+2) = 1: tmp(y+0, x+3) = a
        tmp(y+1, x+0) = 3: tmp(y+1, x+1) = 1: tmp(y+1, x+2) = 3: tmp(y+1, x+3) = 2
        tmp(y+2, x+0) = 0: tmp(y+2, x+1) = 2: tmp(y+2, x+2) = 0: tmp(y+2, x+3) = 1
        tmp(y+3, x+0) = 3: tmp(y+3, x+1) = 4: tmp(y+3, x+2) = 2: tmp(y+3, x+3) = b
        limit -= 1
        HilbertE = True
    End Function
    
    Private Function Place(HW As Pos, tmp(,) As Integer, counts() As Integer, start As Pos, colors As Integer) As Boolean
        Dim stock(5, 4) As LinkedList(Of Pos)
        For i As Integer = 0 To 5
            For j As Integer = 1 To 4
                stock(i, j) = New LinkedList(Of Pos)()
            Next j
        Next i
        Dim cx As Integer = start.Item1
        Dim cy As Integer = start.Item2
        Dim cols(HW.Item2 - 1, HW.Item1 - 1) As Integer
        Dim flag As Boolean = True
        Do
            Dim t As Integer = tmp(cy, cx)
            Dim om1, om2 As Integer
            Select Case t
            Case 0: om1 = cols(cy + 1, cx): om2 = cols(cy, cx + 1)
            Case 1: om1 = cols(cy + 1, cx): om2 = cols(cy, cx - 1)
            Case 2: om1 = cols(cy - 1, cx): om2 = cols(cy, cx - 1)
            Case 3: om1 = cols(cy - 1, cx): om2 = cols(cy, cx + 1)
            Case 4: om1 = cols(cy, cx - 1): om2 = cols(cy, cx + 1)
            Case 5: om1 = cols(cy + 1, cx): om2 = cols(cy - 1, cx)
            Case Else
                Console.Error.WriteLine("wrong t:{0}", t)
                Place = False: Exit Function
            End Select
            Dim seli As Integer = 0
            Dim selc As Integer = 0
            For i As Integer = 1 To 4
                If i = om1 OrElse i = om2 Then Continue For
                If counts(i Or (t << 3)) > selc Then
                    selc = counts(i Or (t << 3))
                    seli = i
                End If
            Next i
            If seli = 0 Then
                If flag AndAlso colors = 2 Then
                    flag = False
                    cx = start.Item1: cy = start.Item2: t = tmp(cy, cx)
                    Dim tc As Integer = cols(cy, cx)
                    For y As Integer = 0 To HW.Item2 - 1
                        For x As Integer = 0 To HW.Item1 - 1
                            If tmp(y, x) < 0 Then Continue For
                            counts(cols(y, x) Or (tmp(y, x) << 3)) += 1
                            cols(y, x) = 0
                        Next x
                    Next y
                    seli = 0
                    For i As Integer = 1 To 4
                        If i = tc Then Continue For
                        If counts(i Or (t << 3)) > 0 Then
                            seli = i: Exit For
                        End If
                    Next i
                    If seli =  0 Then
                        Console.Error.WriteLine("not found")
                        Place = False: Exit Function
                    End If
                ElseIf colors > 2 Then
                    Dim dx1 As Integer = 0, dy1 As Integer = 0
                    Dim dx2 As Integer = 0, dy2 As Integer = 0
                    Select Case t
                    Case 0: dy1 =  1: dx2 =  1
                    Case 1: dy1 =  1: dx2 = -1
                    Case 2: dy1 = -1: dx2 = -1
                    Case 3: dy1 = -1: dx2 =  1
                    Case 4: dx1 = -1: dx2 =  1
                    Case 5: dy1 =  1: dy2 = -1
                    End Select
                    For i As Integer = 1 To 4
                        If i = om1 OrElse i = om2 Then Continue For
                        If stock(t, i).Count = 0 Then Continue For
                        Dim node As LinkedListNode(Of Pos) = stock(t, i).First
                        Do While node IsNot Nothing
                            Dim p As Pos = node.Value
                            Dim om3 As Integer = cols(p.Item2 + dy1, p.Item1 + dx1)
                            Dim om4 As Integer = cols(p.Item2 + dy2, p.Item1 + dx2)
                            For j As Integer = 1 To 4
                                If j = i OrElse j = om3 OrElse j = om4 Then Continue For
                                If counts(j Or (t << 3)) = 0 Then Continue For
                                seli = i
                                counts(i Or (t << 3)) += 1
                                node.List.Remove(node)
                                cols(p.Item2, p.Item1) = j
                                counts(j Or (t << 3)) -= 1
                                stock(t, j).AddLast(p)
                                GoTo OuterLoop
                            Next j
                            node = node.Next
                        Loop
                    Next i
OuterLoop:                    
                    If seli = 0 Then
                        Place = False: Exit Function
                    End IF
                Else
                    Place = False: Exit Function
                End If
            End If
            counts(seli Or (t << 3)) -= 1
            cols(cy, cx) = seli
            stock(t, seli).AddLast(New Pos(cx, cy))
            ' Console.Error.WriteLine("{0}, {1}, {2} {3}", om1, om2, seli, selc)
            Select Case t
            Case 0: If cols(cy, cx + 1) = 0 Then cx += 1 Else cy += 1
            Case 1: If cols(cy, cx - 1) = 0 Then cx -= 1 Else cy += 1
            Case 2: If cols(cy, cx - 1) = 0 Then cx -= 1 Else cy -= 1
            Case 3: If cols(cy, cx + 1) = 0 Then cx += 1 Else cy -= 1
            Case 4: If cols(cy, cx + 1) = 0 Then cx += 1 Else cx -= 1
            Case 5: If cols(cy + 1, cx) = 0 Then cy += 1 Else cy -= 1
            End Select
        Loop Until cx = start.Item1 AndAlso cy = start.Item2
        For y As Integer = 0 To HW.Item2 - 1
            For x As Integer = 0 To HW.Item1 - 1
                If tmp(y, x) < 0 Then Continue For
                tmp(y, x) = cols(y, x) Or (tmp(y, x) << 3)
            Next x
        Next y
        Place = True
    End Function
    
    Dim dfsCount As Integer = 0
    Dim dfsLimit As Integer = 0
    Dim dfsOver  As Boolean = False
    Private Sub InitDfs(Optional ptime As Integer = 400)
        dfsCount = 0
        dfsLimit = Environment.TickCount + 400
        dfsOver  = False
    End Sub
    Dim north() As Pos = { New Pos(0, 1), New Pos(1, 3), New Pos(5, 0) }
    Dim east()  As Pos = { New Pos(1, 2), New Pos(2, 0), New Pos(4, 1) }
    Dim south() As Pos = { New Pos(2, 3), New Pos(3, 1), New Pos(5, 2) }
    Dim west()  As Pos = { New Pos(0, 2), New Pos(3, 0), New Pos(4, 3) }
    Private Function Dfs(tmp(,) As Integer, wires() As Integer, face As Integer, cur As Pos, goal As Pos, bnd As Bounds, wrong As Integer) As Boolean
        ' face: 現在位置から向かう向き 0-N 1-E 2-S 3-W
        ' cur : 現在位置(既に埋まっている)
        ' goal: 目的地(既に埋まってる)
        ' bnd : 探索範囲(x1, y1, x2, y2) x1<=x<=x2, y1<=y<=y2
        Dfs = False
        If dfsOver Then Exit Function
        dfsCount += 1
        If dfsCount > 10000 Then
            Dim now_time As Integer = Environment.TickCount
            If now_time > dfsLimit Then
                dfsOver = True
                Exit Function
            End If
        End If
        Dim p As Pos
        Dim es() As Pos
        Select Case face
        Case 0
            p = New Pos(cur.Item1, cur.Item2 - 1)
            If p.Equals(goal) Then
                Select Case tmp(goal.Item2, goal.Item1)
                Case 0, 1, 5: Dfs = True
                End Select
                Exit Function
            End If
            If p.Item2 < bnd.Item2 Then Exit Function
            es = north
        Case 1
            p = New Pos(cur.Item1 + 1, cur.Item2)
            If p.Equals(goal) Then
                Select Case tmp(goal.Item2, goal.Item1)
                Case 1, 2, 4: Dfs = True
                End Select
                Exit Function
            End If
            If p.Item1 > bnd.Item3 Then Exit Function
            es = east
        Case 2
            p = New Pos(cur.Item1, cur.Item2 + 1)
            If p.Equals(goal) Then
                Select Case tmp(goal.Item2, goal.Item1)
                Case 2, 3, 5: Dfs = True
                End Select
                Exit Function
            End If
            If p.Item2 > bnd.Item4 Then Exit Function
            es = south
        Case 3
            p = New Pos(cur.Item1 - 1, cur.Item2)
            If p.Equals(goal) Then
                Select Case tmp(goal.Item2, goal.Item1)
                Case 0, 3, 4: Dfs = True
                End Select
                Exit Function
            End If
            If p.Item1 < bnd.Item1 Then Exit Function
            es = west
        Case Else
            Exit Function
        End Select
        If tmp(p.Item2, p.Item1) >= 0 Then Exit Function
        Dim aimH As Integer = If(p.Item1 < goal.Item1, 1, If(p.Item1 > goal.Item1, 3, -1))
        Dim aimV As Integer = If(p.Item2 < goal.Item2, 2, If(p.Item2 > goal.Item2, 0, -1))
        tmp(p.Item2, p.Item1) = 99
        For Each e As Pos In es
            If e.Item2 <> aimH AndAlso e.Item2 <> aimV Then Continue For
            If wires(e.Item1) = 0 Then Continue For
            wires(e.Item1) -= 1
            If Dfs(tmp, wires, e.Item2, p, goal, bnd, Math.Max(0, wrong - 1)) Then
                tmp(p.Item2, p.Item1) = e.Item1
                Dfs = True: Exit Function
            End If
            wires(e.Item1) += 1
        Next e
        If wrong < 5 Then
            For Each e As Pos In es
                If e.Item2 = aimH OrElse e.Item2 = aimV Then Continue For
                If wires(e.Item1) = 0 Then Continue For
                wires(e.Item1) -= 1
                If Dfs(tmp, wires, e.Item2, p, goal, bnd, wrong + 1) Then
                    tmp(p.Item2, p.Item1) = e.Item1
                    Dfs = True: Exit Function
                End If
                wires(e.Item1) += 1
            Next e
        End If
        tmp(p.Item2, p.Item1) = -1
    End Function
    
    Private Function ApproachT(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        ApproachT = False
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        
        Dim sx As Integer = W - 1, sy As Integer = 0, sf As Integer = 2
        Dim gx As Integer = 0, gy As Integer = H - 1

        If wires(5) \ (H - 2) > 2 Then
            Dim hr As Integer = (wires(5) \ (H - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim x1 As Integer = i + i + 1
                Dim x2 As Integer = x1 + 1
                tmp(1, x1) = 0
                tmp(1, x2) = 1
                tmp(H - 1, x1) = 2
                tmp(H - 1, x2) = 3
                For y As Integer = 2 To H - 2
                    tmp(y, x1) = 5
                    tmp(y, x2) = 5
                Next y
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(5) -= hr * 2 * (H - 3)
            gx = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (H - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= H Then Exit Do
                For y As Integer = 2 To H - 2 Step 2
                    tmp(y, gx + 1) = 3
                    tmp(y, gx + 2) = 1
                    tmp(y, gx + 3) = 0
                    tmp(y, gx + 4) = 2
                    tmp(y + 1, gx + 1) = 0
                    tmp(y + 1, gx + 2) = 2
                    tmp(y + 1, gx + 3) = 3
                    tmp(y + 1, gx + 4) = 1
                Next y
                tmp(1, gx + 1) = 0: tmp(1, gx + 2) = 4
                tmp(1, gx + 3) = 4: tmp(1, gx + 4) = 1
                tmp(H - 1, gx + 1) = 4: tmp(H - 1, gx + 2) = 2
                tmp(H - 1, gx + 3) = 3: tmp(H - 1, gx + 4) = 4
                wires(0) -= H - 2: wires(1) -= H - 2
                wires(2) -= H - 2: wires(3) -= H - 2
                wires(4) -= 4
                gx += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= H + 2 Then Exit Do
                For y As Integer = 3 To H - 2 Step 2
                    tmp(y, gx + 1) = 3
                    tmp(y, gx + 2) = 1
                    tmp(y, gx + 3) = 0
                    tmp(y, gx + 4) = 2
                    tmp(y + 1, gx + 1) = 0
                    tmp(y + 1, gx + 2) = 2
                    tmp(y + 1, gx + 3) = 3
                    tmp(y + 1, gx + 4) = 1
                Next y
                tmp(0, gx + 1) = 1: tmp(1, gx + 1) = 3
                tmp(0, gx + 2) = 0: tmp(1, gx + 2) = 2
                tmp(0, gx + 3) = 1: tmp(1, gx + 3) = 3
                tmp(0, gx + 4) = 0: tmp(1, gx + 4) = 2
                tmp(2, gx + 1) = 0: tmp(2, gx + 2) = 4
                tmp(2, gx + 3) = 4: tmp(2, gx + 4) = 1
                tmp(H - 1, gx + 1) = 4: tmp(H - 1, gx + 2) = 2
                tmp(H - 1, gx + 3) = 3: tmp(H - 1, gx + 4) = 4
                wires(0) -= H: wires(1) -= H
                wires(2) -= H: wires(3) -= H
                gx += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gx < W - 4 AndAlso wires(4) \ (W - (gx + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(4) \ (W - (gx + 1) - 1) - 1) \ 2
            Dim bt As Integer = W - 4
            If ((H - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim y1 As Integer = i + i + 1
                    Dim y2 As Integer = y1 + 1
                    tmp(y1, bt + 1) = tmp(y1, gx)
                    tmp(y2, bt + 1) = tmp(y2, gx)
                    For x As Integer = gx To bt
                        tmp(y1, x) = 4
                        tmp(y2, x) = 4
                    Next x
                Next i
                wires(5) -= vr * 2: wires(4) -= vr * 2 * (bt - gx + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim y1 As Integer = i + i + 2
                    Dim y2 As Integer = y1 + 1
                    tmp(y1, bt + 1) = tmp(y1, gx)
                    tmp(y2, bt + 1) = tmp(y2, gx)
                    For x As Integer = gx To bt
                        tmp(y1, x) = 4
                        tmp(y2, x) = 4
                    Next x
                Next i
                wires(5) -= vr * 2 + 1: wires(4) -= vr * 2 * (bt - gx + 1)
            End If
        End If
        

        Dim wr As Integer
        If colors = 2 Then
            wr = Math.Min(wires(0) \ 2, Math.Min(wires(1) \ 2, Math.Min(wires(2) \ 2 - 1, wires(3) \ 2)))
        Else
            wr = Math.Min(wires(0), Math.Min(wires(1), Math.Min(wires(2) - 1, wires(3))))
        End If
        Do While wr > 0
            Dim f As Boolean = True
            If W - (gx + 1) > 1 AndAlso H - (sy + 1) > 5 AndAlso wires(5) > 2 Then
                tmp(sy + 1, W - 2) = 0
                tmp(sy + 1, W - 1) = 2
                tmp(sy + 2, W - 2) = 5
                tmp(sy + 3, W - 2) = 3
                tmp(sy + 3, W - 1) = 1
                tmp(sy + 4, W - 1) = 5
                sy += 4
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1: wires(5) -= 2
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If H - (sy + 1) > 1 AndAlso W - (gx + 1) > 5 AndAlso wires(4) > 2 Then
                tmp(H - 1, gx + 1) = 2
                tmp(H - 2, gx + 1) = 0
                tmp(H - 2, gx + 2) = 4
                tmp(H - 2, gx + 3) = 1
                tmp(H - 1, gx + 3) = 3
                tmp(H - 1, gx + 4) = 4
                gx += 4
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1: wires(4) -= 2
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop

        Do While wr > 0
            Dim f As Boolean = True
            If W - (gx + 1) > 1 AndAlso H - (sy + 1) > 3 Then
                tmp(sy + 1, W - 2) = 0
                tmp(sy + 1, W - 1) = 2
                tmp(sy + 2, W - 2) = 3
                tmp(sy + 2, W - 1) = 1
                sy += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If H - (sy + 1) > 1 AndAlso W - (gx + 1) > 3 Then
                tmp(H - 1, gx + 1) = 2
                tmp(H - 2, gx + 1) = 0
                tmp(H - 2, gx + 2) = 1
                tmp(H - 1, gx + 2) = 3
                gx += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop
        
        For y As Integer = sy + 1 To H - 2
            tmp(y, W - 1) = 5
        Next y
        For x As Integer = gx + 1 To W - 2
            tmp(H - 1, x) = 4
        Next x
        tmp(H - 1, W - 1) = 2
        sy = H - 2: gx = W - 1
                
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            Console.Error.WriteLine("found it!")
            ApproachT = True
            Exit Function
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            ApproachT = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: ApproachT = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        For i As Integer = 1 To 10
            InitDfs(200)
            If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
                Console.Error.WriteLine("dfs ... {0}", i)
                ApproachT = True: Exit Function
            End If
            
            If i > 1 AndAlso sy - gy >= H \ 4 AndAlso sy > 5 Then
                For j As Integer = 1 To 4
                    Dim tt As Integer = tmp(sy, sx)
                    If tt < 0 Then
                        ApproachT = False: Exit Function
                    End If
                    tmp(sy, sx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx += 1
                    Case 1: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx -= 1
                    Case 2: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx -= 1
                    Case 3: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx += 1
                    Case 4: If tmp(sy, sx + 1) >= 0 Then sx += 1 Else sx -= 1
                    Case 5: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sy -= 1
                    Case Else: ApproachT = False: Exit Function
                    End Select
                Next j
                Select Case tmp(sy, sx) >> 3
                Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
                Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
                Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
                Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
                Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
                Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
                Case Else: ApproachT = False: Exit Function
                End Select
            Else
                For j As Integer = 1 To 3
                    Dim tt As Integer = tmp(gy, gx)
                    If tt < 0 Then
                        ApproachT = False: Exit Function
                    End If
                    tmp(gy, gx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx += 1
                    Case 1: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx -= 1
                    Case 2: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx -= 1
                    Case 3: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx += 1
                    Case 4: If tmp(gy, gx + 1) >= 0 Then gx += 1 Else gx -= 1
                    Case 5: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gy -= 1
                    Case Else: ApproachT = False: Exit Function
                    End Select
                Next j
            End If
        Next i
        
        If DEBUG_MODE Then
            ApproachT = True: Exit Function
        End If
        
        ApproachT = False
        
    End Function
    
    Private Function ApproachX(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        Dim sx As Integer = 0, sy As Integer = H - 1, sf As Integer = 1
        Dim gx As Integer = W - 1, gy As Integer = 0
        If wires(4) \ (W - 2) > 2 Then
            Dim hr As Integer = (wires(4) \ (W - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim y1 As Integer = i + i + 1
                Dim y2 As Integer = y1 + 1
                tmp(y1, 1) = 0
                tmp(y2, 1) = 3
                tmp(y1, W - 1) = 2
                tmp(y2, W - 1) = 1
                For x As Integer = 2 To W - 2
                    tmp(y1, x) = 4
                    tmp(y2, x) = 4
                Next x
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(4) -= hr * 2 * (W - 3)
            gy = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (W - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W Then Exit Do
                For x As Integer = 2 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 1) = 0: tmp(gy + 2, 1) = 5
                tmp(gy + 3, 1) = 5: tmp(gy + 4, 1) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W - 2: wires(1) -= W - 2
                wires(2) -= W - 2: wires(3) -= W - 2
                wires(5) -= 4
                gy += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W + 2 Then Exit Do
                For x As Integer = 3 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 0) = 3: tmp(gy + 1, 1) = 1
                tmp(gy + 2, 0) = 0: tmp(gy + 2, 1) = 2
                tmp(gy + 3, 0) = 3: tmp(gy + 3, 1) = 1
                tmp(gy + 4, 0) = 0: tmp(gy + 4, 1) = 2
                tmp(gy + 1, 2) = 0: tmp(gy + 2, 2) = 5
                tmp(gy + 3, 2) = 5: tmp(gy + 4, 2) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W: wires(1) -= W
                wires(2) -= W: wires(3) -= W
                gy += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gy < H - 4 AndAlso wires(5) \ (H - (gy + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(5) \ (H - (gy + 1) - 1) - 1) \ 2
            Dim bt As Integer = H - 4
            If ((W - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 1
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2: wires(5) -= vr * 2 * (bt - gy + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 2
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2 + 1: wires(5) -= vr * 2 * (bt - gy + 1)
            End If
        End If
        
        
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            Console.Error.WriteLine("success place")
            p = New Pos(gx, gy)
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            ApproachX = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: ApproachX = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        InitDfs(200)
        If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
            Console.Error.WriteLine("dfs ... ok")
            ApproachX = True: Exit Function
        End If
        
        If DEBUG_MODE Then
            ApproachX = True: Exit Function
        End If
        
        ApproachX = False
        
    End Function
    
    
    Private Function ApproachY(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        Dim sx As Integer = 0, sy As Integer = H - 1, sf As Integer = 1
        Dim gx As Integer = W - 1, gy As Integer = 0
        If wires(4) \ (W - 2) > 2 Then
            Dim hr As Integer = (wires(4) \ (W - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim y1 As Integer = i + i + 1
                Dim y2 As Integer = y1 + 1
                tmp(y1, 1) = 0
                tmp(y2, 1) = 3
                tmp(y1, W - 1) = 2
                tmp(y2, W - 1) = 1
                For x As Integer = 2 To W - 2
                    tmp(y1, x) = 4
                    tmp(y2, x) = 4
                Next x
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(4) -= hr * 2 * (W - 3)
            gy = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (W - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W Then Exit Do
                For x As Integer = 2 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 1) = 0: tmp(gy + 2, 1) = 5
                tmp(gy + 3, 1) = 5: tmp(gy + 4, 1) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W - 2: wires(1) -= W - 2
                wires(2) -= W - 2: wires(3) -= W - 2
                wires(5) -= 4
                gy += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W + 2 Then Exit Do
                For x As Integer = 3 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 0) = 3: tmp(gy + 1, 1) = 1
                tmp(gy + 2, 0) = 0: tmp(gy + 2, 1) = 2
                tmp(gy + 3, 0) = 3: tmp(gy + 3, 1) = 1
                tmp(gy + 4, 0) = 0: tmp(gy + 4, 1) = 2
                tmp(gy + 1, 2) = 0: tmp(gy + 2, 2) = 5
                tmp(gy + 3, 2) = 5: tmp(gy + 4, 2) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W: wires(1) -= W
                wires(2) -= W: wires(3) -= W
                gy += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gy < H - 4 AndAlso wires(5) \ (H - (gy + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(5) \ (H - (gy + 1) - 1) - 1) \ 2
            Dim bt As Integer = H - 4
            If ((W - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 1
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2: wires(5) -= vr * 2 * (bt - gy + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 2
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2 + 1: wires(5) -= vr * 2 * (bt - gy + 1)
            End If
        End If
        
        Dim wr As Integer
        If colors = 2 Then
            wr = Math.Min(wires(0) \ 2, Math.Min(wires(1) \ 2, Math.Min(wires(2) \ 2 - 1, wires(3) \ 2)))
        Else
            wr = Math.Min(wires(0), Math.Min(wires(1), Math.Min(wires(2) - 1, wires(3))))
        End If

        Do While wr > 0
            Dim f As Boolean = True
            If H - (gy + 1) > 1 AndAlso W - (sx + 1) > 3 Then
                tmp(H - 2, sx + 1) = 0
                tmp(H - 1, sx + 1) = 2
                tmp(H - 2, sx + 2) = 1
                tmp(H - 1, sx + 2) = 3
                sx += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If W - (sx + 1) > 1 AndAlso H - (gy + 1) > 3 Then
                tmp(gy + 1, W - 1) = 2
                tmp(gy + 1, W - 2) = 0
                tmp(gy + 2, W - 2) = 3
                tmp(gy + 2, W - 1) = 1
                gy += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop
        
        For x As Integer = sx + 1 To W - 2
            tmp(H - 1, x) = 4
        Next x
        For y As Integer = gy + 1 To H - 2
            tmp(y, W - 1) = 5
        Next y
        tmp(H - 1, W - 1) = 2
        sx = W - 2: gy = H - 1
        
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            Console.Error.WriteLine("found it!")
            ApproachY = True
            Exit Function
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            ApproachY = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: ApproachY = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        For i As Integer = 1 To 3
            InitDfs(200)
            If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
                Console.Error.WriteLine("dfs ... {0}", i)
                ApproachY = True: Exit Function
            End If
            
            If i Mod 2 = 0 Then
                For j As Integer = 1 To 4
                    Dim tt As Integer = tmp(sy, sx)
                    If tt < 0 Then
                        ApproachY = False: Exit Function
                    End If
                    tmp(sy, sx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx += 1
                    Case 1: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx -= 1
                    Case 2: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx -= 1
                    Case 3: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx += 1
                    Case 4: If tmp(sy, sx + 1) >= 0 Then sx += 1 Else sx -= 1
                    Case 5: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sy -= 1
                    Case Else: ApproachY = False: Exit Function
                    End Select
                Next j
                Select Case tmp(sy, sx) >> 3
                Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
                Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
                Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
                Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
                Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
                Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
                Case Else: ApproachY = False: Exit Function
                End Select
            Else
                For j As Integer = 1 To 3
                    Dim tt As Integer = tmp(gy, gx)
                    If tt < 0 Then
                        ApproachY = False: Exit Function
                    End If
                    tmp(gy, gx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx += 1
                    Case 1: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx -= 1
                    Case 2: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx -= 1
                    Case 3: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx += 1
                    Case 4: If tmp(gy, gx + 1) >= 0 Then gx += 1 Else gx -= 1
                    Case 5: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gy -= 1
                    Case Else: ApproachY = False: Exit Function
                    End Select
                Next j
            End If
        Next i
        
        If DEBUG_MODE Then
            ApproachY = True: Exit Function
        End If
        
        ApproachY = False
        
    End Function
    
    
    Private Function ApproachZ(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        Dim sx As Integer = 0, sy As Integer = H - 1, sf As Integer = 1
        Dim gx As Integer = W - 1, gy As Integer = 0
        If wires(4) \ (W - 2) > 2 Then
            Dim hr As Integer = (wires(4) \ (W - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim y1 As Integer = i + i + 1
                Dim y2 As Integer = y1 + 1
                tmp(y1, 1) = 0
                tmp(y2, 1) = 3
                tmp(y1, W - 1) = 2
                tmp(y2, W - 1) = 1
                For x As Integer = 2 To W - 2
                    tmp(y1, x) = 4
                    tmp(y2, x) = 4
                Next x
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(4) -= hr * 2 * (W - 3)
            gy = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (W - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W Then Exit Do
                For x As Integer = 2 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 1) = 0: tmp(gy + 2, 1) = 5
                tmp(gy + 3, 1) = 5: tmp(gy + 4, 1) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W - 2: wires(1) -= W - 2
                wires(2) -= W - 2: wires(3) -= W - 2
                wires(5) -= 4
                gy += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= W + 2 Then Exit Do
                For x As Integer = 3 To W - 2 Step 2
                    tmp(gy + 1, x) = 1
                    tmp(gy + 2, x) = 3
                    tmp(gy + 3, x) = 0
                    tmp(gy + 4, x) = 2
                    tmp(gy + 1, x + 1) = 0
                    tmp(gy + 2, x + 1) = 2
                    tmp(gy + 3, x + 1) = 1
                    tmp(gy + 4, x + 1) = 3
                Next x
                tmp(gy + 1, 0) = 3: tmp(gy + 1, 1) = 1
                tmp(gy + 2, 0) = 0: tmp(gy + 2, 1) = 2
                tmp(gy + 3, 0) = 3: tmp(gy + 3, 1) = 1
                tmp(gy + 4, 0) = 0: tmp(gy + 4, 1) = 2
                tmp(gy + 1, 2) = 0: tmp(gy + 2, 2) = 5
                tmp(gy + 3, 2) = 5: tmp(gy + 4, 2) = 3
                tmp(gy + 1, W - 1) = 5: tmp(gy + 2, W - 1) = 2
                tmp(gy + 3, W - 1) = 1: tmp(gy + 4, W - 1) = 5
                wires(0) -= W: wires(1) -= W
                wires(2) -= W: wires(3) -= W
                gy += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gy < H - 4 AndAlso wires(5) \ (H - (gy + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(5) \ (H - (gy + 1) - 1) - 1) \ 2
            Dim bt As Integer = H - 4
            If ((W - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 1
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2: wires(5) -= vr * 2 * (bt - gy + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim x1 As Integer = i + i + 2
                    Dim x2 As Integer = x1 + 1
                    tmp(bt + 1, x1) = tmp(gy, x1)
                    tmp(bt + 1, x2) = tmp(gy, x2)
                    For y As Integer = gy To bt
                        tmp(y, x1) = 5
                        tmp(y, x2) = 5
                    Next y
                Next i
                wires(4) -= vr * 2 + 1: wires(5) -= vr * 2 * (bt - gy + 1)
            End If
        End If
        
        Dim wr As Integer
        If colors = 2 Then
            wr = Math.Min(wires(0) \ 2, Math.Min(wires(1) \ 2, Math.Min(wires(2) \ 2 - 1, wires(3) \ 2)))
        Else
            wr = Math.Min(wires(0), Math.Min(wires(1), Math.Min(wires(2) - 1, wires(3))))
        End If
        Do While wr > 0
            Dim f As Boolean = True
            If H - (gy + 1) > 1 AndAlso W - (sx + 1) > 5 AndAlso wires(4) > 2 Then
                tmp(H - 2, sx + 1) = 0
                tmp(H - 1, sx + 1) = 2
                tmp(H - 2, sx + 2) = 4
                tmp(H - 2, sx + 3) = 1
                tmp(H - 1, sx + 3) = 3
                tmp(H - 1, sx + 4) = 4
                sx += 4
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1: wires(4) -= 2
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If W - (sx + 1) > 1 AndAlso H - (gy + 1) > 5 AndAlso wires(5) > 2 Then
                tmp(gy + 1, W - 1) = 2
                tmp(gy + 1, W - 2) = 0
                tmp(gy + 2, W - 2) = 5
                tmp(gy + 3, W - 2) = 3
                tmp(gy + 3, W - 1) = 1
                tmp(gy + 4, W - 1) = 5
                gy += 4
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1: wires(5) -= 2
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop

        Do While wr > 0
            Dim f As Boolean = True
            If H - (gy + 1) > 1 AndAlso W - (sx + 1) > 3 Then
                tmp(H - 2, sx + 1) = 0
                tmp(H - 1, sx + 1) = 2
                tmp(H - 2, sx + 2) = 1
                tmp(H - 1, sx + 2) = 3
                sx += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If W - (sx + 1) > 1 AndAlso H - (gy + 1) > 3 Then
                tmp(gy + 1, W - 1) = 2
                tmp(gy + 1, W - 2) = 0
                tmp(gy + 2, W - 2) = 3
                tmp(gy + 2, W - 1) = 1
                gy += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop
        
        Do While sx < W - 2
            If wires(4) <= 0 Then Exit Do
            sx += 1
            tmp(H - 1, sx) = 4
            wires(4) -= 1
        Loop
        Do While gy < H - 2
            If wires(5) <= 0 Then Exit Do
            gy += 1
            tmp(gy, W - 1) = 5
            wires(5) -= 1
        Loop
        If sx = W - 2 AndAlso gy = H - 2
            tmp(H - 1, W - 1) = 2
            gy += 1
        End If
        
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            If sx + 1 = gx AndAlso sy = gy Then
                Console.Error.WriteLine("found it!")
                ApproachZ = True: Exit Function
            End If
            Console.Error.WriteLine("success place")
            p = New Pos(gx, gy)
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            ApproachZ = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: ApproachZ = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        For i As Integer = 1 To 3
            InitDfs(200)
            If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
                Console.Error.WriteLine("dfs ... {0}", i)
                ApproachZ = True: Exit Function
            End If
            
            If i > 1 AndAlso sx - gx >= W \ 4 AndAlso sx > 5 Then
                For j As Integer = 1 To 4
                    Dim tt As Integer = tmp(sy, sx)
                    If tt < 0 Then
                        ApproachZ = False: Exit Function
                    End If
                    tmp(sy, sx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx += 1
                    Case 1: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx -= 1
                    Case 2: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx -= 1
                    Case 3: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx += 1
                    Case 4: If tmp(sy, sx + 1) >= 0 Then sx += 1 Else sx -= 1
                    Case 5: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sy -= 1
                    Case Else: ApproachZ = False: Exit Function
                    End Select
                Next j
                Select Case tmp(sy, sx) >> 3
                Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
                Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
                Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
                Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
                Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
                Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
                Case Else: ApproachZ = False: Exit Function
                End Select
            Else
                For j As Integer = 1 To 3
                    Dim tt As Integer = tmp(gy, gx)
                    If tt < 0 Then
                        ApproachZ = False: Exit Function
                    End If
                    tmp(gy, gx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx += 1
                    Case 1: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx -= 1
                    Case 2: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx -= 1
                    Case 3: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx += 1
                    Case 4: If tmp(gy, gx + 1) >= 0 Then gx += 1 Else gx -= 1
                    Case 5: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gy -= 1
                    Case Else: ApproachZ = False: Exit Function
                    End Select
                Next j
            End If
        Next i
        
        If DEBUG_MODE Then
            ApproachZ = True: Exit Function
        End If
        
        ApproachZ = False
        
    End Function
    
    
    Private Function ApproachS(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        ApproachS = False
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        
        Dim sx As Integer = W - 1, sy As Integer = 0, sf As Integer = 2
        Dim gx As Integer = 0, gy As Integer = H - 1

        If wires(5) \ (H - 2) > 2 Then
            Dim hr As Integer = (wires(5) \ (H - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim x1 As Integer = i + i + 1
                Dim x2 As Integer = x1 + 1
                tmp(1, x1) = 0
                tmp(1, x2) = 1
                tmp(H - 1, x1) = 2
                tmp(H - 1, x2) = 3
                For y As Integer = 2 To H - 2
                    tmp(y, x1) = 5
                    tmp(y, x2) = 5
                Next y
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(5) -= hr * 2 * (H - 3)
            gx = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (H - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= H Then Exit Do
                For y As Integer = 2 To H - 2 Step 2
                    tmp(y, gx + 1) = 3
                    tmp(y, gx + 2) = 1
                    tmp(y, gx + 3) = 0
                    tmp(y, gx + 4) = 2
                    tmp(y + 1, gx + 1) = 0
                    tmp(y + 1, gx + 2) = 2
                    tmp(y + 1, gx + 3) = 3
                    tmp(y + 1, gx + 4) = 1
                Next y
                tmp(1, gx + 1) = 0: tmp(1, gx + 2) = 4
                tmp(1, gx + 3) = 4: tmp(1, gx + 4) = 1
                tmp(H - 1, gx + 1) = 4: tmp(H - 1, gx + 2) = 2
                tmp(H - 1, gx + 3) = 3: tmp(H - 1, gx + 4) = 4
                wires(0) -= H - 2: wires(1) -= H - 2
                wires(2) -= H - 2: wires(3) -= H - 2
                wires(4) -= 4
                gx += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= H + 2 Then Exit Do
                For y As Integer = 3 To H - 2 Step 2
                    tmp(y, gx + 1) = 3
                    tmp(y, gx + 2) = 1
                    tmp(y, gx + 3) = 0
                    tmp(y, gx + 4) = 2
                    tmp(y + 1, gx + 1) = 0
                    tmp(y + 1, gx + 2) = 2
                    tmp(y + 1, gx + 3) = 3
                    tmp(y + 1, gx + 4) = 1
                Next y
                tmp(0, gx + 1) = 1: tmp(1, gx + 1) = 3
                tmp(0, gx + 2) = 0: tmp(1, gx + 2) = 2
                tmp(0, gx + 3) = 1: tmp(1, gx + 3) = 3
                tmp(0, gx + 4) = 0: tmp(1, gx + 4) = 2
                tmp(2, gx + 1) = 0: tmp(2, gx + 2) = 4
                tmp(2, gx + 3) = 4: tmp(2, gx + 4) = 1
                tmp(H - 1, gx + 1) = 4: tmp(H - 1, gx + 2) = 2
                tmp(H - 1, gx + 3) = 3: tmp(H - 1, gx + 4) = 4
                wires(0) -= H: wires(1) -= H
                wires(2) -= H: wires(3) -= H
                gx += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gx < W - 4 AndAlso wires(4) \ (W - (gx + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(4) \ (W - (gx + 1) - 1) - 1) \ 2
            Dim bt As Integer = W - 4
            If ((H - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim y1 As Integer = i + i + 1
                    Dim y2 As Integer = y1 + 1
                    tmp(y1, bt + 1) = tmp(y1, gx)
                    tmp(y2, bt + 1) = tmp(y2, gx)
                    For x As Integer = gx To bt
                        tmp(y1, x) = 4
                        tmp(y2, x) = 4
                    Next x
                Next i
                wires(5) -= vr * 2: wires(4) -= vr * 2 * (bt - gx + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim y1 As Integer = i + i + 2
                    Dim y2 As Integer = y1 + 1
                    tmp(y1, bt + 1) = tmp(y1, gx)
                    tmp(y2, bt + 1) = tmp(y2, gx)
                    For x As Integer = gx To bt
                        tmp(y1, x) = 4
                        tmp(y2, x) = 4
                    Next x
                Next i
                wires(5) -= vr * 2 + 1: wires(4) -= vr * 2 * (bt - gx + 1)
            End If
        End If
        

        Dim wr As Integer
        If colors = 2 Then
            wr = Math.Min(wires(0) \ 2, Math.Min(wires(1) \ 2, Math.Min(wires(2) \ 2 - 1, wires(3) \ 2)))
        Else
            wr = Math.Min(wires(0), Math.Min(wires(1), Math.Min(wires(2) - 1, wires(3))))
        End If

        Do While wr > 0
            Dim f As Boolean = True
            If W - (gx + 1) > 1 AndAlso H - (sy + 1) > 3 Then
                tmp(sy + 1, W - 2) = 0
                tmp(sy + 1, W - 1) = 2
                tmp(sy + 2, W - 2) = 3
                tmp(sy + 2, W - 1) = 1
                sy += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If H - (sy + 1) > 1 AndAlso W - (gx + 1) > 3 Then
                tmp(H - 1, gx + 1) = 2
                tmp(H - 2, gx + 1) = 0
                tmp(H - 2, gx + 2) = 1
                tmp(H - 1, gx + 2) = 3
                gx += 2
                wires(0) -= 1: wires(1) -= 1: wires(2) -= 1: wires(3) -= 1
                f = False
                wr -= 1
                If wr <= 0 Then Exit Do
            End If
            If f Then Exit Do
        Loop
        
        For y As Integer = sy + 1 To H - 2
            tmp(y, W - 1) = 5
        Next y
        For x As Integer = gx + 1 To W - 2
            tmp(H - 1, x) = 4
        Next x
        tmp(H - 1, W - 1) = 2
        sy = H - 2: gx = W - 1
                
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            Console.Error.WriteLine("found it!")
            ApproachS = True
            Exit Function
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            ApproachS = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: ApproachS = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        For i As Integer = 1 To 3
            InitDfs(200)
            If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
                Console.Error.WriteLine("dfs ... {0}", i)
                ApproachS = True: Exit Function
            End If
            
            If i Mod 2 = 0 Then
                For j As Integer = 1 To 4
                    Dim tt As Integer = tmp(sy, sx)
                    If tt < 0 Then
                        ApproachS = False: Exit Function
                    End If
                    tmp(sy, sx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx += 1
                    Case 1: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sx -= 1
                    Case 2: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx -= 1
                    Case 3: If tmp(sy - 1, sx) >= 0 Then sy -= 1 Else sx += 1
                    Case 4: If tmp(sy, sx + 1) >= 0 Then sx += 1 Else sx -= 1
                    Case 5: If tmp(sy + 1, sx) >= 0 Then sy += 1 Else sy -= 1
                    Case Else: ApproachS = False: Exit Function
                    End Select
                Next j
                Select Case tmp(sy, sx) >> 3
                Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
                Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
                Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
                Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
                Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
                Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
                Case Else: ApproachS = False: Exit Function
                End Select
            Else
                For j As Integer = 1 To 3
                    Dim tt As Integer = tmp(gy, gx)
                    If tt < 0 Then
                        ApproachS = False: Exit Function
                    End If
                    tmp(gy, gx) = -1
                    counts(tt) += 1
                    Select Case tt >> 3
                    Case 0: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx += 1
                    Case 1: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gx -= 1
                    Case 2: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx -= 1
                    Case 3: If tmp(gy - 1, gx) >= 0 Then gy -= 1 Else gx += 1
                    Case 4: If tmp(gy, gx + 1) >= 0 Then gx += 1 Else gx -= 1
                    Case 5: If tmp(gy + 1, gx) >= 0 Then gy += 1 Else gy -= 1
                    Case Else: ApproachS = False: Exit Function
                    End Select
                Next j
            End If
        Next i
        
        If DEBUG_MODE Then
            ApproachS = True: Exit Function
        End If
        
        ApproachS = False
        
    End Function
    
    
    Private Function ApproachR(H As Integer, W As Integer, colors As Integer, _
            tmp(,) As Integer, counts() As Integer, wires() As Integer, mins() As Integer) As Boolean
        If colors = 2 Then
            For i As Integer = 0 To 5
                wires(i) = mins(i) * 2
            Next i
        End If
        
        ApproachR = False
        
        tmp(0, 0) = 0
        tmp(0, W - 1) = 1
        For i As Integer = 1 To W - 2
            tmp(0, i) = 4
        Next i
        For j As Integer = 1 To H - 2
            tmp(j, 0) = 5
        Next j
        tmp(H - 1, 0) = 3
        wires(0) -= 1: wires(1) -= 1: wires(3) -= 1
        wires(4) -= W - 2: wires(5) -= H - 2
        
        Dim sx As Integer = W - 1, sy As Integer = 0, sf As Integer = 2
        Dim gx As Integer = 0, gy As Integer = H - 1

        If wires(5) \ (H - 2) > 2 Then
            Dim hr As Integer = (wires(5) \ (H - 2) - 1) \ 2
            For i As Integer = 0 To hr - 1
                Dim x1 As Integer = i + i + 1
                Dim x2 As Integer = x1 + 1
                tmp(1, x1) = 0
                tmp(1, x2) = 1
                tmp(H - 1, x1) = 2
                tmp(H - 1, x2) = 3
                For y As Integer = 2 To H - 2
                    tmp(y, x1) = 5
                    tmp(y, x2) = 5
                Next y
            Next i
            wires(0) -= hr: wires(1) -= hr: wires(2) -= hr: wires(3) -= hr
            wires(5) -= hr * 2 * (H - 3)
            gx = hr * 2
        End If
        
        Dim zg As Integer = 0
        If (H - 3) Mod 2 <> 0 Then
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= H Then Exit Do
                For y As Integer = 2 To H - 2 Step 2
                    tmp(y, gx + 1) = 3
                    tmp(y, gx + 2) = 1
                    tmp(y, gx + 3) = 0
                    tmp(y, gx + 4) = 2
                    tmp(y + 1, gx + 1) = 0
                    tmp(y + 1, gx + 2) = 2
                    tmp(y + 1, gx + 3) = 3
                    tmp(y + 1, gx + 4) = 1
                Next y
                tmp(1, gx + 1) = 0: tmp(1, gx + 2) = 4
                tmp(1, gx + 3) = 4: tmp(1, gx + 4) = 1
                tmp(H - 1, gx + 1) = 4: tmp(H - 1, gx + 2) = 2
                tmp(H - 1, gx + 3) = 3: tmp(H - 1, gx + 4) = 4
                wires(0) -= H - 2: wires(1) -= H - 2
                wires(2) -= H - 2: wires(3) -= H - 2
                wires(4) -= 4
                gx += 4
                zg += 1
            Loop
        Else
            Dim minwr As Integer = H * W
            Do
                For i As Integer = 0 To 3
                    minwr = Math.Min(minwr, wires(i))
                Next i
                If minwr <= H + 2 Then Exit Do
                For y As Integer = 3 To H - 2 Step 2
                    tmp(y, gx + 1) = 3
                    tmp(y, gx + 2) = 1
                    tmp(y, gx + 3) = 0
                    tmp(y, gx + 4) = 2
                    tmp(y + 1, gx + 1) = 0
                    tmp(y + 1, gx + 2) = 2
                    tmp(y + 1, gx + 3) = 3
                    tmp(y + 1, gx + 4) = 1
                Next y
                tmp(0, gx + 1) = 1: tmp(1, gx + 1) = 3
                tmp(0, gx + 2) = 0: tmp(1, gx + 2) = 2
                tmp(0, gx + 3) = 1: tmp(1, gx + 3) = 3
                tmp(0, gx + 4) = 0: tmp(1, gx + 4) = 2
                tmp(2, gx + 1) = 0: tmp(2, gx + 2) = 4
                tmp(2, gx + 3) = 4: tmp(2, gx + 4) = 1
                tmp(H - 1, gx + 1) = 4: tmp(H - 1, gx + 2) = 2
                tmp(H - 1, gx + 3) = 3: tmp(H - 1, gx + 4) = 4
                wires(0) -= H: wires(1) -= H
                wires(2) -= H: wires(3) -= H
                gx += 4
                zg += 1
            Loop            
        End If
        
        If zg > 0 AndAlso gx < W - 4 AndAlso wires(4) \ (W - (gx + 1) - 1) > 2 Then
            Dim vr As Integer = (wires(4) \ (W - (gx + 1) - 1) - 1) \ 2
            Dim bt As Integer = W - 4
            If ((H - 3) Mod 2 <> 0) Then
                For i As Integer = 0 To vr - 1
                    Dim y1 As Integer = i + i + 1
                    Dim y2 As Integer = y1 + 1
                    tmp(y1, bt + 1) = tmp(y1, gx)
                    tmp(y2, bt + 1) = tmp(y2, gx)
                    For x As Integer = gx To bt
                        tmp(y1, x) = 4
                        tmp(y2, x) = 4
                    Next x
                Next i
                wires(5) -= vr * 2: wires(4) -= vr * 2 * (bt - gx + 1)
            Else
                For i As Integer = 0 To vr - 1
                    Dim y1 As Integer = i + i + 2
                    Dim y2 As Integer = y1 + 1
                    tmp(y1, bt + 1) = tmp(y1, gx)
                    tmp(y2, bt + 1) = tmp(y2, gx)
                    For x As Integer = gx To bt
                        tmp(y1, x) = 4
                        tmp(y2, x) = 4
                    Next x
                Next i
                wires(5) -= vr * 2 + 1: wires(4) -= vr * 2 * (bt - gx + 1)
            End If
        End If
        
        Dim p As Pos = TryPlace(H, W, colors, tmp, counts, sf, New Pos(sx, sy), New Pos(gx, gy))
        
        If p Is Nothing Then
            Console.Error.WriteLine("success place")
            p = New Pos(gx, gy)
        End If
        
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 > 0 Then
            Select Case tmp(p.Item2, p.Item1 - 1) >> 3
            Case 0, 3, 4: p = New Pos(p.Item1 - 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 > 0 Then
            Select Case tmp(p.Item2 - 1, p.Item1) >> 3
            Case 0, 1, 5: p = New Pos(p.Item1, p.Item2 - 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item1 + 1 < W Then
            Select Case tmp(p.Item2, p.Item1 + 1) >> 3
            Case 1, 2, 4: p = New Pos(p.Item1 + 1, p.Item2)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 AndAlso p.Item2 + 1 < H Then
            Select Case tmp(p.Item2 + 1, p.Item1) >> 3
            Case 2, 3, 5: p = New Pos(p.Item1, p.Item2 + 1)
            End Select
        End If
        If tmp(p.Item2, p.Item1) < 0 Then
            ApproachR = False: Exit Function
        End If
        
        Dim fc As Integer = -1
        Select Case tmp(sy, sx) >> 3
        Case 0: fc = If(tmp(sy + 1, sx) < 0, 2, 1)
        Case 1: fc = If(tmp(sy + 1, sx) < 0, 2, 3)
        Case 2: fc = If(tmp(sy - 1, sx) < 0, 0, 3)
        Case 3: fc = If(tmp(sy - 1, sx) < 0, 0, 1)
        Case 4: fc = If(tmp(sy, sx + 1) < 0, 1, 3)
        Case 5: fc = If(tmp(sy + 1, sx) < 0, 2, 0)
        Case Else: ApproachR = False: Exit Function
        End Select
        
        gx = p.Item1: gy = p.Item2
        
        InitDfs(200)
        If Dfs(0, H, W, tmp, counts, fc, sx, sy, New Pos(gx, gy), 0) Then
            Console.Error.WriteLine("dfs ... ok")
            ApproachR = True: Exit Function
        End If
            
        If DEBUG_MODE Then
            ApproachR = True: Exit Function
        End If
        
        ApproachR = False
        
    End Function
    
    
End Class