Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System
Imports System.Collections.Generic
Imports System.IO

Public Module Research

	Public Sub Main()
		Try
			Dim H As Integer = CInt(Console.ReadLine())
			Dim W As Integer = CInt(Console.ReadLine())
			Dim lights(H * W - 1) As String
			For i As Integer = 0 To UBound(lights)
				lights(i) = Console.ReadLine()
			Next i
			
            Dim cols As New SortedDictionary(Of Char, Integer)()
            Dim hm As New SortedDictionary(Of String, Integer)()
            For Each v As String In lights
                If hm.ContainsKey(v) Then
                    hm(v) += 1
                Else
                    hm(v) = 1
                End If
                If cols.ContainsKey(v(0)) Then
                    cols(v(0)) += 1
                Else
                    cols(v(0)) = 1
                End If
                If cols.ContainsKey(v(1)) Then
                    cols(v(1)) += 1
                Else
                    cols(v(1)) = 1
                End If
            Next v
            
			Dim seed As Integer = CInt(Environment.GetCommandLineArgs()(1))
            
            Dim sw As New StringWriter()
            
			sw.WriteLine( _
				"seed: {0:D8}, [H: {1,3:D}], [W: {2,3:D}], [C: {3}]" _
				, seed, H, W, cols.Count - 6)
            sw.Write("parts: ")
            For Each kv As KeyValuePair(Of Char, Integer) In cols
                sw.Write("[{0}:{1,4:D}], ", kv.Key, kv.Value)
            Next kv
            sw.WriteLine()
            sw.Write("tiles: ")
            For Each kv As KeyValuePair(Of String, Integer) In hm
                sw.Write("[{0}:{1,4:D}], ", kv.Key, kv.Value)
            Next kv
            sw.WriteLine()
			
            Console.Error.WriteLine(sw)
            
			Dim ret(H * W - 1) As Integer
			For i As Integer = 0 To UBound(ret)
				ret(i) = i
			Next i

			Console.WriteLine(ret.Length)
			For Each r As Integer In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module