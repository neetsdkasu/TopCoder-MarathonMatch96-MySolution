Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim H As Integer = CInt(Console.ReadLine())
			Dim W As Integer = CInt(Console.ReadLine())
			Dim lights(H * W - 1) As String
			For i As Integer = 0 To UBound(lights)
				lights(i) = Console.ReadLine()
			Next i
			
			Dim gl As New GarlandOfLights()
			Dim ret() As Integer = gl.create(H, W, lights)

			Console.WriteLine(ret.Length)
			For Each r As Integer In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module